/* linux/arch/arm/mach-msm/board-htcraphael.c
 *
 * Copyright (C) 2007 Google, Inc.
 * Author: Brian Swetland <swetland@google.com>,
 * Octavian Voicu, Martijn Stolk
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/input.h>

#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <linux/i2c.h>
#include <linux/mm.h>

#include <mach/hardware.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/flash.h>
#include <asm/mach/mmc.h>
#include <asm/setup.h>

#include <mach/board.h>
#include <mach/msm_iomap.h>
#include <mach/system.h>
#include <mach/msm_fb.h>
#include <mach/msm_hsusb.h>
#include <mach/msm_serial_hs.h>
#include <mach/vreg.h>
#include <mach/htc_battery.h>
#include <mach/htc_headset_mgr.h>
#include <mach/htc_headset_gpio.h>

#include <mach/gpio.h>
#include <mach/io.h>
#include <linux/delay.h>
#include <linux/gpio_keys.h>


#include <linux/microp-keypad.h>
#include <mach/board_htc.h>

#include "proc_comm_wince.h"
#include "devices.h"
#include "htc_hw.h"
#include "board-htcblackstone.h"
#include "htc-usb.h"

static int halibut_ffa;
module_param_named(ffa, halibut_ffa, int, S_IRUGO | S_IWUSR | S_IWGRP);
static int adb=0;
module_param(adb, int, S_IRUGO | S_IWUSR | S_IWGRP);

static void blackstone_device_specific_fixes(void);

extern int init_mmc(void);
extern void msm_init_pmic_vibrator(void);

static int usb_phy_init_seq_raph100[] = {
	0x40, 0x31, /* Leave this pair out for USB Host Mode */
	0x1D, 0x0D,
	0x1D, 0x10,
	-1
};

static void usb_phy_shutdown(void)
{
        gpio_set_value(0x64, 1);
        gpio_set_value(0x64, 0);
}

static void usb_phy_reset(void)
{
	usb_phy_shutdown();
	gpio_set_value(0x64, 0);
	mdelay(1);
	gpio_set_value(0x64, 1);
	mdelay(3);
	htc_usb_ulpi_config(1);
}

#if defined(CONFIG_MSM_CAMERA) && defined(CONFIG_MT9P012)

static struct msm_gpio_config camera_off_gpio_table[] = {
	/* CAMERA */
    DEX_GPIO_CFG(0, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT0 */
	DEX_GPIO_CFG(1, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT1 */
	DEX_GPIO_CFG(2, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT2 */
	DEX_GPIO_CFG(3, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT3 */
	DEX_GPIO_CFG(4, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT4 */
	DEX_GPIO_CFG(5, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT5 */
	DEX_GPIO_CFG(6, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT6 */
	DEX_GPIO_CFG(7, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT7 */
	DEX_GPIO_CFG(8, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT8 */
	DEX_GPIO_CFG(9, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT9 */
	//DEX_GPIO_CFG(10, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT10 */
	//DEX_GPIO_CFG(11, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT11 */
	DEX_GPIO_CFG(12, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* PCLK */
	DEX_GPIO_CFG(13, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* HSYNC_IN */
	DEX_GPIO_CFG(14, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* VSYNC_IN */
	DEX_GPIO_CFG(15, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* MCLK */
};

static struct msm_gpio_config camera_on_gpio_table[] = {
	/* CAMERA */
    DEX_GPIO_CFG(0, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT0 */
	DEX_GPIO_CFG(1, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT1 */
	DEX_GPIO_CFG(2, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT2 */
	DEX_GPIO_CFG(3, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT3 */
	DEX_GPIO_CFG(4, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT4 */
	DEX_GPIO_CFG(5, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT5 */
	DEX_GPIO_CFG(6, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT6 */
	DEX_GPIO_CFG(7, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT7 */
	DEX_GPIO_CFG(8, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT8 */
	DEX_GPIO_CFG(9, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT9 */
	//DEX_GPIO_CFG(10, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT10 */
	//DEX_GPIO_CFG(11, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT11 */
	DEX_GPIO_CFG(12, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* PCLK */
	DEX_GPIO_CFG(13, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* HSYNC_IN */
	DEX_GPIO_CFG(14, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* VSYNC_IN */
	DEX_GPIO_CFG(15, 1, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA, 0), /* MCLK */
};

static void config_gpio_table(struct msm_gpio_config *table, int len)
{
	int n;
	struct msm_gpio_config id;
	for(n = 0; n < len; n++) {
		id = table[n];
		msm_gpio_set_function( id );
	}
}

static void config_camera_on_gpios(int Gpio30Value)
{
	struct msm_dex_command dex = { .cmd = PCOM_PMIC_REG_ON, .has_data=1 };

    /* VDD 1.8V */
	dex.data=0x100;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDD 2.65V */
	dex.data=0x400000;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDD 2.85V */
	dex.data=0x20;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);

    /* Select camera (front or back) */
	gpio_direction_output(BLAC100_CAM_PWR1, Gpio30Value);

	config_gpio_table(camera_on_gpio_table,
		ARRAY_SIZE(camera_on_gpio_table));
}

static void config_camera_off_gpios(void)
{
	struct msm_dex_command dex = { .cmd = PCOM_PMIC_REG_OFF, .has_data=1 };

    /* VDD 1.8V */
	dex.data=0x100;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDD 2.65V */
	dex.data=0x400000;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDD 2.85V */
	dex.data=0x20;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);

    /* Reset camera select gpio to main (back) camera */
	gpio_direction_output(BLAC100_CAM_PWR1, 1);

	config_gpio_table(camera_off_gpio_table,
		ARRAY_SIZE(camera_off_gpio_table));
}

#ifdef CONFIG_MT9P012
static void config_camera_on_gpios_mt9p012(void)
{
        config_camera_on_gpios(0);
}
#endif

#endif

static struct i2c_board_info i2c_devices[] = {
	{
		// LED & Backlight controller
		I2C_BOARD_INFO("microp-klt", 0x66),
	},
#if defined(CONFIG_MSM_CAMERA) && defined(CONFIG_MT9P012)
	{		
		I2C_BOARD_INFO("mt9p012", 0x6c >> 1),
	},
#endif
	{		
		I2C_BOARD_INFO("kionix-kxsd9", 0x19),
	},
};

static smem_batt_t msm_battery_pdata = {
        .gpio_battery_detect = BLAC100_BAT_IRQ,
        .gpio_charger_enable = BLAC100_CHARGE_EN_N,
        .gpio_charger_current_select = BLAC100_USB_AC_PWR,
        .smem_offset = 0xfc110,
        .smem_field_size = 2,
};

static struct platform_device raphael_rfkill = {
	.name = "htcraphael_rfkill",
	.id = -1,
};

#define SND(num, desc) { .name = desc, .id = num }
static struct snd_endpoint snd_endpoints_list[] = {
	SND(0, "HANDSET"),
	SND(1, "SPEAKER"),
	SND(2, "HEADSET"),
	SND(3, "BT"),
	SND(44, "BT_EC_OFF"),
	SND(10, "HEADSET_AND_SPEAKER"),
	SND(256, "CURRENT"),

	/* Bluetooth accessories. */

	SND(12, "HTC BH S100"),
	SND(13, "HTC BH M100"),
	SND(14, "Motorola H500"),
	SND(15, "Nokia HS-36W"),
	SND(16, "PLT 510v.D"),
	SND(17, "M2500 by Plantronics"),
	SND(18, "Nokia HDW-3"),
	SND(19, "HBH-608"),
	SND(20, "HBH-DS970"),
	SND(21, "i.Tech BlueBAND"),
	SND(22, "Nokia BH-800"),
	SND(23, "Motorola H700"),
	SND(24, "HTC BH M200"),
	SND(25, "Jabra JX10"),
	SND(26, "320Plantronics"),
	SND(27, "640Plantronics"),
	SND(28, "Jabra BT500"),
	SND(29, "Motorola HT820"),
	SND(30, "HBH-IV840"),
	SND(31, "6XXPlantronics"),
	SND(32, "3XXPlantronics"),
	SND(33, "HBH-PV710"),
	SND(34, "Motorola H670"),
	SND(35, "HBM-300"),
	SND(36, "Nokia BH-208"),
	SND(37, "Samsung WEP410"),
	SND(38, "Jabra BT8010"),
	SND(39, "Motorola S9"),
	SND(40, "Jabra BT620s"),
	SND(41, "Nokia BH-902"),
	SND(42, "HBH-DS220"),
	SND(43, "HBH-DS980"),
};
#undef SND

static struct msm_snd_endpoints blac_snd_endpoints = {
        .endpoints = snd_endpoints_list,
        .num = ARRAY_SIZE(snd_endpoints_list),
};

static struct platform_device blac_snd = {
	.name = "msm_snd",
	.id = -1,
	.dev	= {
		.platform_data = &blac_snd_endpoints,
	},
};

static struct platform_device touchscreen = {
	.name		= "tssc-manager",
	.id		= -1,
};


static struct htc_headset_mgr_platform_data htc_headset_mgr_data = {
};

static struct platform_device htc_headset_mgr = {
	.name   = "HTC_HEADSET_MGR",
    .id     = -1,
    .dev    = {
		.platform_data  = &htc_headset_mgr_data,
    },
};

static struct htc_headset_gpio_platform_data htc_headset_gpio_data = {
	.hpin_gpio              = BLAC100_GPIO_HDS_DET,
	.mic_detect_gpio        = RAPH100_EXTMIC_IRQ,
	.microp_channel         = 1,
	.key_enable_gpio        = 0,
	.mic_select_gpio        = 0,
};

static struct platform_device htc_headset_gpio = {
    .name   = "HTC_HEADSET_GPIO",
    .id     = -1,
    .dev    = {
		.platform_data  = &htc_headset_gpio_data,
	},
};

#ifdef CONFIG_MSM_CAMERA

#ifdef CONFIG_MT9P012
static struct msm_camera_device_platform_data msm_camera_device_data_mt9p012 = {
	.camera_gpio_on  = config_camera_on_gpios_mt9p012,
	.camera_gpio_off = config_camera_off_gpios,
	.ioext.mdcphy = MSM_MDC_PHYS,
	.ioext.mdcsz  = MSM_MDC_SIZE,
	.ioext.appphy = MSM_CLK_CTL_PHYS,
	.ioext.appsz  = MSM_CLK_CTL_SIZE,
};

static struct msm_camera_sensor_info msm_camera_sensor_mt9p012_data = {
	.sensor_name    = "mt9p012",
	.sensor_reset   = BLAC100_CAM_RSTMAIN,
//	.sensor_pwd     = BLAC100_CAM_PWR1,
	.vcm_pwd        = BLAC100_CAM_VCMPDP,
	.pdata          = &msm_camera_device_data_mt9p012,
};

static struct platform_device msm_camera_sensor_mt9p012 = {
	.name           = "msm_camera_mt9p012",
	.dev            = {
		.platform_data = &msm_camera_sensor_mt9p012_data,
	},
};
#endif

#endif

static struct platform_device *devices[] __initdata = {
	&raphael_rfkill,
	&msm_device_smd,
	&msm_device_nand,
	&msm_device_i2c,
	&msm_device_rtc,
	&msm_device_htc_hw,
	&touchscreen,
#ifdef CONFIG_SERIAL_MSM_HS
	&msm_device_uart_dm2,
#endif
	&msm_device_htc_battery,
	&blac_snd,
	&htc_headset_mgr,
    &htc_headset_gpio,
#ifdef CONFIG_MT9P012
	&msm_camera_sensor_mt9p012,
#endif
};

extern struct sys_timer msm_timer;

static void __init halibut_init_irq(void)
{
	msm_init_irq();
}

static struct msm_acpu_clock_platform_data halibut_clock_data = {
	.acpu_switch_time_us = 50,
	.max_speed_delta_khz = 256000,
	.vdd_switch_time_us = 62,
	.power_collapse_khz = 19200,
	.wait_for_irq_khz = 128000,
};

void msm_serial_debug_init(unsigned int base, int irq, 
			   const char *clkname, int signal_irq);

#ifdef CONFIG_SERIAL_MSM_HS
static struct msm_serial_hs_platform_data msm_uart_dm2_pdata = {
	.wakeup_irq = MSM_GPIO_TO_INT(21),
	.inject_rx_on_wakeup = 1,
	.rx_to_inject = 0x32,
};
#endif

static htc_hw_pdata_t msm_htc_hw_pdata = {
	.set_vibrate = msm_proc_comm_wince_vibrate,
	.battery_smem_offset = 0xfc110, //XXX: raph800
	.battery_smem_field_size = 2,
};

static void __init halibut_init(void)
{
	// Fix data in arrays depending on GSM/CDMA version
	blackstone_device_specific_fixes();

	msm_acpu_clock_init(&halibut_clock_data);
	msm_proc_comm_wince_init();

	msm_device_htc_hw.dev.platform_data = &msm_htc_hw_pdata;
	msm_device_htc_battery.dev.platform_data = &msm_battery_pdata;
	msm_add_usb_devices(usb_phy_reset, NULL, usb_phy_init_seq_raph100);

#ifdef CONFIG_SERIAL_MSM_HS
	msm_device_uart_dm2.dev.platform_data = &msm_uart_dm2_pdata;
#endif

#ifndef CONFIG_MACH_SAPPHIRE
	msm_init_pmic_vibrator();
#endif

	platform_add_devices(devices, ARRAY_SIZE(devices));
	i2c_register_board_info(0, i2c_devices, ARRAY_SIZE(i2c_devices));
	init_mmc();

	/* TODO: detect vbus and correctly notify USB about its presence 
	 * For now we just declare that VBUS is present at boot and USB
	 * copes, but this is not ideal.
	 */
	msm_hsusb_set_vbus_state(1);

	msm_proc_comm_wince_vibrate_welcome();
}

static void __init halibut_map_io(void)
{
	msm_map_common_io();
	msm_clock_init();
}

static void __init blac_fixup(struct machine_desc *desc, struct tag *tags,
                                    char **cmdline, struct meminfo *mi)
{
	mi->nr_banks = 1;
	mi->bank[0].start = PAGE_ALIGN(PHYS_OFFSET);
	mi->bank[0].node = PHYS_TO_NID(mi->bank[0].start);
	mi->bank[0].size = 107*1024*1024;
	mi->nr_banks++;
	mi->bank[1].start = PAGE_ALIGN(PHYS_OFFSET + 0x10000000);
	mi->bank[1].node = PHYS_TO_NID(mi->bank[1].start);
	mi->bank[1].size = (128 * 1024 * 1024)-50*1024*1024; // See pmem.c
	printk(KERN_INFO "fixup: nr_banks = %d\n", mi->nr_banks);
	printk(KERN_INFO "fixup: bank0 start=%08lx, node=%08x, size=%08lx\n", mi->bank[0].start, mi->bank[0].node, mi->bank[0].size);
	printk(KERN_INFO "fixup: bank1 start=%08lx, node=%08x, size=%08lx\n", mi->bank[1].start, mi->bank[1].node, mi->bank[1].size);
}

static void blackstone_device_specific_fixes(void)
{
	msm_htc_hw_pdata.battery_smem_offset = 0xfc110;
	msm_htc_hw_pdata.battery_smem_field_size = 2;
	msm_battery_pdata.smem_offset = 0xfc110;
	msm_battery_pdata.smem_field_size = 2;
}

MACHINE_START(HTCBLACKSTONE, "HTC blackstone cellphone (aka HTC Touch HD)")
	.fixup 		= blac_fixup,
	.boot_params	= 0x10000100,
	.map_io		= halibut_map_io,
	.init_irq	= halibut_init_irq,
	.init_machine	= halibut_init,
	.timer		= &msm_timer,
MACHINE_END
