/* linux/arch/arm/mach-msm/board-htcraphael.h
 */
#ifndef __ARCH_ARM_MACH_MSM_BOARD_HTCRAPHAEL_H
#define __ARCH_ARM_MACH_MSM_BOARD_HTCRAPHAEL_H

#include <mach/board.h>
/* GPIO */

#define RAPH100_H2W_PWR		0       // <- used for camera ?!?
#define BLAC100_USB_AC_PWR	1       // <- used for camera ?!?

#define BLAC100_CAM_DAT0	0
#define BLAC100_CAM_DAT1	1
#define BLAC100_CAM_DAT2	2
#define BLAC100_CAM_DAT3	3
#define BLAC100_CAM_DAT4	4
#define BLAC100_CAM_DAT5	5
#define BLAC100_CAM_DAT6	6
#define BLAC100_CAM_DAT7	7
#define BLAC100_CAM_DAT8	8
#define BLAC100_CAM_DAT9	9
#define BLAC100_CAM_DAT10	10
#define BLAC100_CAM_DAT11	11
#define BLAC100_CAM_PCLK	12
#define BLAC100_CAM_HSYNC_IN	13
#define BLAC100_CAM_VSYNC_N	14
#define BLAC100_CAM_MCLK	15
/*				16*/
#define RAPH100_EXTMIC_IRQ	17
#define RAPH100_CABLE_IN_IRQ	18
#define RAPH100_UART2_TX	19	/* BT */
#define RAPH100_UART2_RX	20	/* BT */
#define RAPH100_UART2_CTS	21	/* BT*/
/*				22*/
#define RAPH100_SD_DETECT	23
/*				24*/
#define RAPH100_ARM9_SOFTRST	25
/*				26*/
#define RAPH100_KBD_IRQ		27
#define BLAC100_BAT_IRQ		28
#define RAPH100_WIFI_IRQ	29
#define BLAC100_CAM_PWR1	30
/*	H2W			31*/
#define RAPH100_KPD_ROW0	32
#define BLAC100_CAM_VCMPDP  33
#define BLAC100_CAM_RSTMAIN 34
#define BLAC100_CAM_RSTVID  35
#define RAPH100_RST_IRQ		36
/*	SD detect too ???	37*/
#define RAPH100_CLAMSHELL_IRQ	38
/*	unused			39*/
#define RAPH100_KPD_COL0	40
#define RAPH100_KPD_COL1	41
#define RAPH100_KPD_COL2	42
/*	region_id something	43*/
#define BLAC100_CHARGE_EN_N	44
#define RAPH100_UART1_TX	45	/* DBG */
#define RAPH100_UART1_RX	46	/* DBG */
/*				47*/
/*				48*/
/*				49*/
/*				50*/
#define RAPH100_WIFI_DAT3	51
#define RAPH100_WIFI_DAT2	52
#define RAPH100_WIFI_DAT1	53
#define RAPH100_WIFI_DAT0	54
#define RAPH100_WIFI_CMD	55
#define RAPH100_WIFI_CLK	56
#define RAPH100_TVOUT		57
#define RAPH100_FLASHLIGHT	58
/*				59*/
#define RAPH100_I2C_SCL		60
#define RAPH100_I2C_SDA		61
#define RAPH100_SD_CLK		62
#define RAPH100_SD_CMD		63
#define RAPH100_SD_DAT3		64
#define RAPH100_SD_DAT2		65
#define RAPH100_SD_DAT1		66
#define RAPH100_SD_DAT0		67
#define RAPH100_BTAUDIO0	68
#define RAPH100_BTAUDIO1	69
#define RAPH100_BTAUDIO2	70
#define RAPH100_BTAUDIO3	71

#define RAPH100_LCD_PWR4	76
#define RAPH100_USBCHARGE_EN	77

#define RAPH100_LCD_PWR3	82
#define RAPH100_PWR_BTN		83

#define RAPH100_BKL_PWR		86

#define RAPH100_HP_PWR		89

//#define RAPH100_CAM1_RST	92
//#define RAPH100_CAM2_RST	93
#define BLAC100_GPIO_HDS_DET	94

#define RAPH100_LCD_VSYNC	97 /* FIXME! hardcoded */
#define RAPH100_LCD_PWR1	98
#define RAPH100_LCD_PWR2	99

#define RAPH100_USBPHY_RST	100
#define RAPH100_BT_RST		101
#define RAPH100_WIFI_PWR1	102
#define RAPH100_WIFI_PWR2	103

#define RAPH100_UART2_RTS	108

/* 111 - 121 USB OTG */

#endif /* GUARD */
