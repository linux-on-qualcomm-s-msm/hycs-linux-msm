/* linux/arch/arm/mach-msm/board-htcraphael.c
 *
 * Copyright (C) 2007 Google, Inc.
 * Author: Brian Swetland <swetland@google.com>,
 * Octavian Voicu, Martijn Stolk
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/input.h>

#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <linux/i2c.h>
#include <linux/mm.h>

#include <mach/hardware.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/flash.h>
#include <asm/mach/mmc.h>
#include <asm/setup.h>

#include <mach/board.h>
#include <mach/msm_iomap.h>
#include <mach/system.h>
#include <mach/msm_fb.h>
#include <mach/msm_hsusb.h>
#include <mach/msm_serial_hs.h>
#include <mach/vreg.h>
#include <mach/htc_battery.h>

#include <mach/gpio.h>
#include <mach/io.h>
#include <mach/board_htc.h>
#include <linux/delay.h>
#ifdef CONFIG_HTC_HEADSET
#include <mach/htc_headset.h>
#endif

#include <linux/microp-keypad.h>
#include <linux/gpio_keys.h>

#include "proc_comm_wince.h"
#include "devices.h"
#include "htc_hw.h"
#include "board-htcraphael.h"
#include "htc-usb.h"

static int adb=0;
module_param(adb, int, S_IRUGO | S_IWUSR | S_IWGRP);

static void htcraphael_device_specific_fixes(void);

extern int init_mmc(void);
extern void msm_init_pmic_vibrator(void);

static struct resource raphael_keypad_resources[] = {
	{ 
		.start = MSM_GPIO_TO_INT(RAPH100_KPD_IRQ), /* Modified in htcraphael_device_specific_fixes() */
		.end = MSM_GPIO_TO_INT(RAPH100_KPD_IRQ), /* Modified in htcraphael_device_specific_fixes() */
		.flags = IORESOURCE_IRQ,
	},
};

static struct microp_keypad_platform_data raphael_keypad_data = {
	.clamshell = { 
		.gpio = RAPH800_CLAMSHELL_IRQ, /* Modified in htcraphael_device_specific_fixes() */
		.irq = MSM_GPIO_TO_INT(RAPH800_CLAMSHELL_IRQ), /* Modified in htcraphael_device_specific_fixes() */
	},
	.backlight_gpio = RAPH100_BKL_PWR, /* Modified in htcraphael_device_specific_fixes() */
};

static struct platform_device raphael_keypad_device = {
	.name = "microp-keypad",
	.id = 0,
	.num_resources = ARRAY_SIZE(raphael_keypad_resources),
	.resource = raphael_keypad_resources,
	.dev = { .platform_data = &raphael_keypad_data, },
};


static int usb_phy_init_seq_raph100[] = {
	0x40, 0x31, /* Leave this pair out for USB Host Mode */
	0x1D, 0x0D,
	0x1D, 0x10,
	-1
};

static int usb_phy_init_seq_raph800[] = {
	0x04, 0x48, /* Host mode is unsure for raph800 */
	0x3A, 0x10,
	0x3B, 0x10,
	-1
};

static void usb_phy_shutdown(void)
{
	gpio_set_value(RAPH100_USBPHY_RST, 1);
	mdelay(3);
	gpio_set_value(RAPH100_USBPHY_RST, 0);
	mdelay(3);
}

static void usb_phy_reset(void)
{
	usb_phy_shutdown();
	gpio_set_value(RAPH100_USBPHY_RST, 0);
	mdelay(1);
	gpio_set_value(RAPH100_USBPHY_RST, 1);
	mdelay(3);
	/* cannot enable ulpi yet. Need to get correct settings.
	 Will need to boot with usb from wince to use */ 
	//htc_usb_ulpi_config(1);
}

#if defined(CONFIG_MSM_CAMERA) && ( defined(CONFIG_MT9T013) || defined(CONFIG_MT9V113) )

static struct msm_gpio_config camera_off_gpio_table[] = {
	/* CAMERA */
	DEX_GPIO_CFG(2, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT2 */
	DEX_GPIO_CFG(3, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT3 */
	DEX_GPIO_CFG(4, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT4 */
	DEX_GPIO_CFG(5, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT5 */
	DEX_GPIO_CFG(6, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT6 */
	DEX_GPIO_CFG(7, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT7 */
	DEX_GPIO_CFG(8, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT8 */
	DEX_GPIO_CFG(9, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT9 */
	DEX_GPIO_CFG(10, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT10 */
	DEX_GPIO_CFG(11, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT11 */
	DEX_GPIO_CFG(12, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* PCLK */
	DEX_GPIO_CFG(13, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* HSYNC_IN */
	DEX_GPIO_CFG(14, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* VSYNC_IN */
	DEX_GPIO_CFG(15, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA, 0), /* MCLK */
};

static struct msm_gpio_config camera_on_gpio_table[] = {
	/* CAMERA */
	DEX_GPIO_CFG(2, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT2 */
	DEX_GPIO_CFG(3, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT3 */
	DEX_GPIO_CFG(4, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT4 */
	DEX_GPIO_CFG(5, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT5 */
	DEX_GPIO_CFG(6, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT6 */
	DEX_GPIO_CFG(7, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT7 */
	DEX_GPIO_CFG(8, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT8 */
	DEX_GPIO_CFG(9, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT9 */
	DEX_GPIO_CFG(10, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT10 */
	DEX_GPIO_CFG(11, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT11 */
	DEX_GPIO_CFG(12, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* PCLK */
	DEX_GPIO_CFG(13, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* HSYNC_IN */
	DEX_GPIO_CFG(14, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* VSYNC_IN */
	DEX_GPIO_CFG(15, 1, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA, 0), /* MCLK */
};

static void config_gpio_table(struct msm_gpio_config *table, int len)
{
	int n;
	struct msm_gpio_config id;
	for(n = 0; n < len; n++) {
		id = table[n];
		msm_gpio_set_function( id );
	}
}

static void config_camera_on_gpios(int bFrontCam)
{
    struct msm_dex_command dex = { .cmd = PCOM_PMIC_REG_ON, .has_data=1 };
    int SKUID_SKU;
    int cam_pwr_gpio;

    /* VDD 1.8V */
    dex.data=0x100;
    msm_proc_comm_wince(&dex,0);
    mdelay(20);
    if (machine_is_htcraphael() ) {
        /* VDD 2.65V */
        dex.data=0x400000;
        msm_proc_comm_wince(&dex,0);
        mdelay(20);
    } else {
        /* VDDIO 2.65V */
        dex.data=0x200000;
        msm_proc_comm_wince(&dex,0);
        mdelay(20);
        /* VCM VDDD 1.8V */
        dex.data=0x8000;
        msm_proc_comm_wince(&dex,0);
        mdelay(20);
    }
    /* VDDA 2.85V */
    dex.data=0x20;
    msm_proc_comm_wince(&dex,0);
    mdelay(20);

    /* Configure common camera pins */
    config_gpio_table(camera_on_gpio_table,
        ARRAY_SIZE(camera_on_gpio_table));

    msm_gpio_set_function( DEX_GPIO_CFG(107, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );
    if (machine_is_htcraphael() ) {
        SKUID_SKU = readb( MSM_SPL_BASE + 0x81C04 ) ;
        if ( (SKUID_SKU > 1) && (SKUID_SKU != 0xFF) ) {
            cam_pwr_gpio = RAPH100_CAM_PWR1;
        } else {
            cam_pwr_gpio = RAPH100_CAM_RST_CDMA;
        }

        /* Select camera (front or back) */
        msm_gpio_set_function( DEX_GPIO_CFG(cam_pwr_gpio      , 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, bFrontCam) );
        msm_gpio_set_function( DEX_GPIO_CFG(RAPH100_CAM1_RST  , 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );
        msm_gpio_set_function( DEX_GPIO_CFG(RAPH100_CAM2_RST  , 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );
        msm_gpio_set_function( DEX_GPIO_CFG(RAPH100_CAM_VCMPDP, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 1) );
    } else {
        msm_gpio_set_function( DEX_GPIO_CFG(RAPH100_CAM_RST_CDMA, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );
    }
}

static void config_camera_off_gpios(void)
{
    struct msm_dex_command dex = { .cmd = PCOM_PMIC_REG_OFF, .has_data=1 };
    int SKUID_SKU;
    int cam_pwr_gpio;

    if (machine_is_htcraphael() ) {
        /* VDD 1.8V */
        dex.data=0x100;
        msm_proc_comm_wince(&dex,0);
        mdelay(20);
        /* VDD 2.65V */
        dex.data=0x400000;
        msm_proc_comm_wince(&dex,0);
        mdelay(20);
        /* VDD 2.85V */
        dex.data=0x20;
        msm_proc_comm_wince(&dex,0);
        mdelay(20);

        /* Reset camera select gpio to main (back) camera */
#if 1
        SKUID_SKU = readb( MSM_SPL_BASE + 0x81C04 ) ;
        if ( (SKUID_SKU > 1) && (SKUID_SKU != 0xFF) ) {
            cam_pwr_gpio = RAPH100_CAM_PWR1;
        } else {
            cam_pwr_gpio = RAPH100_CAM_RST_CDMA;
        }
	    gpio_direction_output(cam_pwr_gpio, 1);
#else
	    gpio_direction_output(RAPH100_CAM_PWR1, 1);
#endif

        /* Turn off flashlight : TODO : confirm for all devices */
        //msm_gpio_set_function( DEX_GPIO_CFG(58, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );
    } else {
        /* VCM VDDD 1.8V */
        dex.data=0x8000;
        msm_proc_comm_wince(&dex,0);
        mdelay(20);
        /* VDDA 2.85V */
        dex.data=0x20;
        msm_proc_comm_wince(&dex,0);
        mdelay(20);
        /* VDDIO 2.65V */
        dex.data=0x200000;
        msm_proc_comm_wince(&dex,0);
        mdelay(20);
        /* VDDD 1.8V */
        dex.data=0x100;
        msm_proc_comm_wince(&dex,0);
        mdelay(20);
    }

    config_gpio_table(camera_off_gpio_table,
        ARRAY_SIZE(camera_off_gpio_table));

    msm_gpio_set_function( DEX_GPIO_CFG(107, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );
}

#ifdef CONFIG_MT9T013
static void config_camera_on_gpios_mt9t013(void)
{
        config_camera_on_gpios(0);
}
#endif

#ifdef CONFIG_MT9V113
static void config_camera_on_gpios_mt9v113(void)
{
        config_camera_on_gpios(1);
}
#endif

#endif

#ifdef CONFIG_MSM_CAMERA

#ifdef CONFIG_MT9T013
static int raphael_camera_flash(int level)
{
    int HwBoardID = readb( MSM_SPL_BASE + 0x81030 ) ;
    int FlashLightGPIO0, FlashLightGPIO1;

    if ( machine_is_htcraphael_cdma() || machine_is_htcraphael_cdma500() ) {
        FlashLightGPIO0 = RAPH800_FLASHLIGHT0;
        FlashLightGPIO1 = RAPH800_FLASHLIGHT1;
    } else {
        FlashLightGPIO0 = RAPH100_FLASHLIGHT0;
        if (HwBoardID >= 1) {
            FlashLightGPIO1 = RAPH100_FLASHLIGHT1;
        } else {
            FlashLightGPIO1 = RAPH100_FLASHLIGHT2;
        }
    }

    printk("%s : level = %d\n", __func__, level);

    switch(level) {
        case 0:

        break;

        case 1:
        case 5:
            msm_gpio_set_function( DEX_GPIO_CFG(FlashLightGPIO0, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 1) );
        break;

        case 2:
        case 6:
            msm_gpio_set_function( DEX_GPIO_CFG(FlashLightGPIO0, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0) );
        break;

        case 3:
            msm_gpio_set_function( DEX_GPIO_CFG(FlashLightGPIO1, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 1) );
        break;

        case 4:
            msm_gpio_set_function( DEX_GPIO_CFG(FlashLightGPIO1, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0) );
        break;

    };

    return level;
}

static struct camera_flash_cfg raphael_flash = {
    .num_flash_levels = 6,
    .camera_flash     = raphael_camera_flash,
};

static struct msm_camera_device_platform_data msm_camera_device_data_mt9t013 = {
	.camera_gpio_on  = config_camera_on_gpios_mt9t013,
	.camera_gpio_off = config_camera_off_gpios,
	.ioext.mdcphy = MSM_MDC_PHYS,
	.ioext.mdcsz  = MSM_MDC_SIZE,
	.ioext.appphy = MSM_CLK_CTL_PHYS,
	.ioext.appsz  = MSM_CLK_CTL_SIZE,
};

static struct msm_camera_sensor_info msm_camera_sensor_mt9t013_data = {
	.sensor_name    = "mt9t013",
	.sensor_reset   = RAPH100_CAM1_RST,
//	.sensor_pwd     = RAPH100_CAM_PWR1, // Only used for ov8810 & ov9665 sensors
	.vcm_pwd        = RAPH100_CAM_VCMPDP,
	.pdata          = &msm_camera_device_data_mt9t013,
    .flash_type     = MSM_CAMERA_FLASH_LED,
    .flash_cfg      = &raphael_flash,
};

static struct platform_device msm_camera_sensor_mt9t013 = {
	.name           = "msm_camera_mt9t013",
	.dev            = {
		.platform_data = &msm_camera_sensor_mt9t013_data,
	},
};

static void htcraphael_camera_fixup(void)
{
    if ( machine_is_htcraphael_cdma() || machine_is_htcraphael_cdma500() ) {
        msm_camera_sensor_mt9t013_data.sensor_reset = RAPH100_CAM_RST_CDMA; // Gpio 109 used for reset
        msm_camera_sensor_mt9t013_data.vcm_pwd = 0; // Not used

    }
}

#endif

#ifdef CONFIG_MT9V113
static struct msm_camera_device_platform_data msm_camera_device_data_mt9v113 = {
	.camera_gpio_on  = config_camera_on_gpios_mt9v113,
	.camera_gpio_off = config_camera_off_gpios,
	.ioext.mdcphy = MSM_MDC_PHYS,
	.ioext.mdcsz  = MSM_MDC_SIZE,
	.ioext.appphy = MSM_CLK_CTL_PHYS,
	.ioext.appsz  = MSM_CLK_CTL_SIZE,
};

static struct msm_camera_sensor_info msm_camera_sensor_mt9v113_data = {
	.sensor_name    = "mt9v113",
	.sensor_reset   = RAPH100_CAM2_RST,
	.sensor_pwd     = RAPH100_CAM_PWR1,
	.vcm_pwd        = 0, // 0 means not used
	.pdata          = &msm_camera_device_data_mt9v113,
};

static struct platform_device msm_camera_sensor_mt9v113 = {
	.name           = "msm_camera_mt9v113",
	.dev            = {
		.platform_data = &msm_camera_sensor_mt9v113_data,
	},
};
#endif

#endif

static struct i2c_board_info i2c_devices[] = {
	{
		// LED & Backlight controller
		I2C_BOARD_INFO("microp-klt", 0x66),
	},
	{
		// Keyboard controller
		I2C_BOARD_INFO("microp-ksc", 0x67),
	},
#if defined(CONFIG_MSM_CAMERA) && defined(CONFIG_MT9T013)
	{
		I2C_BOARD_INFO("mt9t013", 0x6C),
	},
#endif
#if defined(CONFIG_MSM_CAMERA) && defined(CONFIG_MT9V113)
	{
		I2C_BOARD_INFO("mt9v113", 0x78),
	},
#endif
	{
		// Raphael NaviPad (cy8c20434)
		I2C_BOARD_INFO("raph_navi_pad", 0x62),
	},
	{
		// Accelerometer
		I2C_BOARD_INFO("kionix-kxsd9", 0x18),
	},
};

static smem_batt_t msm_battery_pdata = {
	.gpio_battery_detect = RAPH100_BAT_IRQ,
	.gpio_charger_enable = RAPH100_CHARGE_EN_N,
	.gpio_charger_current_select = RAPH100_USB_AC_PWR,
	.smem_offset = 0xfc140,
	.smem_field_size = 4,
};

static struct platform_device raphael_rfkill = {
	.name = "htcraphael_rfkill",
	.id = -1,
};

#define SND(num, desc) { .name = desc, .id = num }
static struct snd_endpoint snd_endpoints_list[] = {
	SND(0, "HANDSET"),
	SND(1, "SPEAKER"),
	SND(2, "HEADSET"),
	SND(3, "BT"),
};
#undef SND

static struct msm_snd_endpoints raphael_snd_endpoints = {
        .endpoints = snd_endpoints_list,
        .num = ARRAY_SIZE(snd_endpoints_list),
};

static struct platform_device raphael_snd = {
	.name = "msm_snd",
	.id = -1,
	.dev	= {
		.platform_data = &raphael_snd_endpoints,
	},
};

#ifdef CONFIG_HTC_HEADSET

static void h2w_config_cpld(int route);
static void h2w_init_cpld(void);
static struct h2w_platform_data raphael_h2w_data = {
	.cable_in1              = 18,//TROUT_GPIO_CABLE_IN1,
	.cable_in2              = 45,//TROUT_GPIO_CABLE_IN2,
	.h2w_clk                = 46,//TROUT_GPIO_H2W_CLK_GPI,
	.h2w_data               = 31,//TROUT_GPIO_H2W_DAT_GPI,
	.debug_uart             = H2W_UART3,
	.config_cpld            = h2w_config_cpld,
	.init_cpld              = h2w_init_cpld,
	.headset_mic_35mm       = 17,
};

static void h2w_config_cpld(int route)
{
	switch (route) {
		case H2W_UART3:
			gpio_set_value(0, 1);
			break;
		case H2W_GPIO:
			gpio_set_value(0, 0);
			break;
	}
}

static void h2w_init_cpld(void)
{
	h2w_config_cpld(H2W_UART3);
	gpio_set_value(raphael_h2w_data.h2w_clk, 0);
	gpio_set_value(raphael_h2w_data.h2w_data, 0);
}

static struct platform_device raphael_h2w = {
	.name           = "h2w",
	.id             = -1,
	.dev            = {
		.platform_data  = &raphael_h2w_data,
	},
};
#endif

static struct platform_device touchscreen = {
	.name		= "tssc-manager",
	.id		= -1,
};

static struct platform_device *devices[] __initdata = {
	&raphael_keypad_device,
	&raphael_rfkill,
	&msm_device_smd,
	&msm_device_nand,
	&msm_device_i2c,
	&msm_device_rtc,
	&msm_device_htc_hw,
#if !defined(CONFIG_MSM_SERIAL_DEBUGGER) && !defined(CONFIG_HTC_HEADSET)
//	&msm_device_uart1,
#endif
#ifdef CONFIG_SERIAL_MSM_HS
	&msm_device_uart_dm2,
#endif
	&msm_device_htc_battery,
	&raphael_snd,
	&touchscreen,
#ifdef CONFIG_HTC_HEADSET
	&raphael_h2w,
#endif
#ifdef CONFIG_MT9T013
	&msm_camera_sensor_mt9t013,
#endif
};

extern struct sys_timer msm_timer;

static void __init halibut_init_irq(void)
{
	msm_init_irq();
}

static struct msm_acpu_clock_platform_data halibut_clock_data = {
	.acpu_switch_time_us = 50,
	.max_speed_delta_khz = 256000,
	.vdd_switch_time_us = 62,
	.power_collapse_khz = 19200,
	.wait_for_irq_khz = 128000,
};

void msm_serial_debug_init(unsigned int base, int irq, 
			   const char *clkname, int signal_irq);

#ifdef CONFIG_SERIAL_MSM_HS
static struct msm_serial_hs_platform_data msm_uart_dm2_pdata = {
	.wakeup_irq = MSM_GPIO_TO_INT(RAPH100_UART2DM_RX),
	.inject_rx_on_wakeup = 1,
	.rx_to_inject = 0x32,
};
#endif

static htc_hw_pdata_t msm_htc_hw_pdata = {
	.set_vibrate = msm_proc_comm_wince_vibrate,
	.battery_smem_offset = 0xfc140, //XXX: raph800
	.battery_smem_field_size = 4,
};

static void __init halibut_init(void)
{
	// Fix data in arrays depending on GSM/CDMA version
	htcraphael_device_specific_fixes();
#if defined(CONFIG_MSM_CAMERA) && defined(CONFIG_MT9T013)
    htcraphael_camera_fixup();
#endif
	msm_acpu_clock_init(&halibut_clock_data);
	msm_proc_comm_wince_init();

	// Device pdata overrides
	msm_device_htc_hw.dev.platform_data = &msm_htc_hw_pdata;
	msm_device_htc_battery.dev.platform_data = &msm_battery_pdata;

	if(machine_is_htcraphael())
		msm_add_usb_devices(usb_phy_reset, NULL, usb_phy_init_seq_raph100);		
	else
		msm_add_usb_devices(usb_phy_reset, NULL, usb_phy_init_seq_raph800);

	

#ifdef CONFIG_SERIAL_MSM_HS
	msm_device_uart_dm2.dev.platform_data = &msm_uart_dm2_pdata;
#endif

	msm_init_pmic_vibrator();

	// Register devices
	platform_add_devices(devices, ARRAY_SIZE(devices));

	// Register I2C devices
	i2c_register_board_info(0, i2c_devices, ARRAY_SIZE(i2c_devices));

#if defined(CONFIG_MSM_CAMERA) && defined(CONFIG_MT9V113)
    if (!machine_is_htcraphael()) {
        platform_add_device(&msm_camera_sensor_mt9v113);
    }
#endif

	// Initialize SD controllers
	init_mmc();

	/* TODO: detect vbus and correctly notify USB about its presence 
	 * For now we just declare that VBUS is present at boot and USB
	 * copes, but this is not ideal.
	 */
	msm_hsusb_set_vbus_state(1);

	msm_proc_comm_wince_vibrate_welcome();
}

static void __init halibut_map_io(void)
{
	msm_map_common_io();
	msm_clock_init();
}

static void __init htcraphael_fixup(struct machine_desc *desc, struct tag *tags,
                                    char **cmdline, struct meminfo *mi)
{
	mi->nr_banks = 1;
	mi->bank[0].start = PAGE_ALIGN(PHYS_OFFSET);
	mi->bank[0].node = PHYS_TO_NID(mi->bank[0].start);
	mi->bank[0].size = 107*1024*1024;
	
	mi->nr_banks++;
	mi->bank[1].start = PAGE_ALIGN(PHYS_OFFSET + 0x10000000);
	mi->bank[1].node = PHYS_TO_NID(mi->bank[1].start);
	mi->bank[1].size = (128 * 1024 * 1024)-50*1024*1024; // See pmem.c

	printk(KERN_INFO "fixup: nr_banks = %d\n", mi->nr_banks);
	printk(KERN_INFO "fixup: bank0 start=%08lx, node=%08x, size=%08lx\n", mi->bank[0].start, mi->bank[0].node, mi->bank[0].size);
	printk(KERN_INFO "fixup: bank1 start=%08lx, node=%08x, size=%08lx\n", mi->bank[1].start, mi->bank[1].node, mi->bank[1].size);
}

static void __init htcraphael_cdma500_fixup(struct machine_desc *desc, struct tag *tags,
                                    char **cmdline, struct meminfo *mi)
{
	mi->nr_banks = 1;
	mi->bank[0].start = PAGE_ALIGN(PHYS_OFFSET);
	mi->bank[0].node = PHYS_TO_NID(mi->bank[0].start);
	mi->bank[0].size = (106 * 1024 * 1024); // Why 106? See pmem.c
	//mi->nr_banks++;
	mi->bank[1].start = PAGE_ALIGN(0x02000000+(14+2+8+8)*1024*1024);
	mi->bank[1].node = PHYS_TO_NID(mi->bank[1].start);
	mi->bank[1].size = (32-2/*fb*/-14/*pmem*/-8/*adsp*/-8/*camera*/)*1024*1024;
	printk(KERN_INFO "fixup: nr_banks = %d\n", mi->nr_banks);
	printk(KERN_INFO "fixup: bank0 start=%08lx, node=%08x, size=%08lx\n", mi->bank[0].start, mi->bank[0].node, mi->bank[0].size);
	if(mi->nr_banks==2)
		printk(KERN_INFO "fixup: bank1 start=%08lx, node=%08x, size=%08lx\n", mi->bank[1].start, mi->bank[1].node, mi->bank[1].size);
}


static void htcraphael_device_specific_fixes(void)
{
	if (machine_is_htcraphael() ) {
		raphael_keypad_resources[0].start = MSM_GPIO_TO_INT(RAPH100_KPD_IRQ);
		raphael_keypad_resources[0].end = MSM_GPIO_TO_INT(RAPH100_KPD_IRQ);
		raphael_keypad_data.clamshell.gpio = RAPH100_CLAMSHELL_IRQ;
		raphael_keypad_data.clamshell.irq = MSM_GPIO_TO_INT(RAPH100_CLAMSHELL_IRQ);
		raphael_keypad_data.backlight_gpio = RAPH100_BKL_PWR;
		msm_htc_hw_pdata.battery_smem_offset = 0xfc110;
		msm_htc_hw_pdata.battery_smem_field_size = 2;
		msm_battery_pdata.smem_offset = 0xfc110;
		msm_battery_pdata.smem_field_size = 2;
	}
	if (machine_is_htcraphael_cdma()) {
		raphael_keypad_resources[0].start = MSM_GPIO_TO_INT(RAPH100_KPD_IRQ);
		raphael_keypad_resources[0].end = MSM_GPIO_TO_INT(RAPH100_KPD_IRQ);
		raphael_keypad_data.clamshell.gpio = RAPH800_CLAMSHELL_IRQ;
		raphael_keypad_data.clamshell.irq = MSM_GPIO_TO_INT(RAPH800_CLAMSHELL_IRQ);
		raphael_keypad_data.backlight_gpio = RAPH100_BKL_PWR;
		msm_htc_hw_pdata.battery_smem_offset = 0xfc140;
		msm_htc_hw_pdata.battery_smem_field_size = 4;
		msm_battery_pdata.smem_offset = 0xfc140;
		msm_battery_pdata.smem_field_size = 4;
	}
	if (machine_is_htcraphael_cdma500()) {
		/* copied from raph800, fix the bt rfkill */
                raphael_keypad_resources[0].start = MSM_GPIO_TO_INT(RAPH100_KPD_IRQ);
                raphael_keypad_resources[0].end = MSM_GPIO_TO_INT(RAPH100_KPD_IRQ);
                raphael_keypad_data.clamshell.gpio = RAPH800_CLAMSHELL_IRQ;
                raphael_keypad_data.clamshell.irq = MSM_GPIO_TO_INT(RAPH800_CLAMSHELL_IRQ);
                raphael_keypad_data.backlight_gpio = RAPH100_BKL_PWR;
                msm_htc_hw_pdata.battery_smem_offset = 0xfc140;
                msm_htc_hw_pdata.battery_smem_field_size = 4;
                msm_battery_pdata.smem_offset = 0xfc140;
                msm_battery_pdata.smem_field_size = 4;
	}

}

MACHINE_START(HTCRAPHAEL, "HTC Raphael GSM phone (aka HTC Touch Pro)")
	.fixup 		= htcraphael_fixup,
	.boot_params	= 0x10000100,
	.map_io		= halibut_map_io,
	.init_irq	= halibut_init_irq,
	.init_machine	= halibut_init,
	.timer		= &msm_timer,
MACHINE_END

MACHINE_START(HTCRAPHAEL_CDMA, "HTC Raphael CDMA phone (aka HTC Touch Pro)")
	.fixup 		= htcraphael_fixup,
	.boot_params	= 0x10000100,
	.map_io		= halibut_map_io,
	.init_irq	= halibut_init_irq,
	.init_machine	= halibut_init,
	.timer		= &msm_timer,
MACHINE_END

MACHINE_START(HTCRAPHAEL_CDMA500, "HTC Raphael CDMA phone (Touch Pro) raph500")
	.fixup		= htcraphael_cdma500_fixup,
	.boot_params	= 0x10000100,
	.map_io		= halibut_map_io,
	.init_irq	= halibut_init_irq,
	.init_machine	= halibut_init,
	.timer		= &msm_timer,
MACHINE_END

