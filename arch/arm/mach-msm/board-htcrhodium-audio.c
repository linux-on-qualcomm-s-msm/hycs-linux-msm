#include <asm/mach-types.h>
#include <asm/io.h>
#include <asm/gpio.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/i2c.h>
#include <linux/microp-klt.h>
#include <linux/hrtimer.h>
#include <linux/delay.h>
#include <linux/debugfs.h>
#include <linux/earlysuspend.h>
#ifdef CONFIG_ANDROID_POWER
#include <linux/android_power.h>
#endif

#include <mach/tpa2016d2.h>
#include "board-htcrhodium.h"

#define MODULE_NAME "rhodium_audio"

struct data_t {
	struct i2c_client *a1010;
	struct i2c_client *adc3001;
};
static struct data_t _dat;


#define IC_REG		0x1
#define ATK_REG		0x2
#define REL_REG		0x3
#define HOLD_REG	0x4
#define FIXED_GAIN_REG	0x5
#define AGC_REG1	0x6
#define AGC_REG2	0x7

#define SPK_EN_L 	1<<6
#define SPK_EN_R 	1<<7

void tpa_set_power(uint8_t arg) {
	char buf;
	tpa2016d2_read_register(IC_REG, &buf);
	buf&=~0xc0;//power settings are two last bits
	buf|=arg;
	tpa2016d2_set_register(IC_REG, buf);
}

void tpa_set_attack_time(uint8_t arg) {
	//Only 6 lower bits
	tpa2016d2_set_register(ATK_REG, arg & 0x3F);
}

void tpa_set_release_time(uint8_t arg) {
	//Only 6 lower bits
	tpa2016d2_set_register(REL_REG, arg & 0x3F);
}

void tpa_set_hold_time(uint8_t arg) {
	//Only 6 lower bits
	tpa2016d2_set_register(HOLD_REG, arg & 0x3F);
}

void tpa_set_fixed_gain(uint8_t arg) {
	//Only 6 lower bits
	tpa2016d2_set_register(FIXED_GAIN_REG, arg & 0x3F);
}

void tpa_set_output_limiter(uint8_t arg) {
	char buf;
	tpa2016d2_read_register(AGC_REG1, &buf);
	buf&=0xf0;//output limiter is the lowest 4 bits
	//0xff = disable output limiter
	if(arg==0xff)
		buf&=~(1<<7);
	else {
		arg&=0xf;
		buf|=arg;
	}
	tpa2016d2_set_register(AGC_REG1, buf);
}

void tpa_set_noise_gate_threshold(uint8_t arg) {
	char buf;
	tpa2016d2_read_register(AGC_REG1, &buf);
	buf&=0x9f;//noise gate threshold is the {5,6} bits
	arg&=0x3;
	buf|=arg<<5;
	tpa2016d2_set_register(AGC_REG1, buf);
}

void tpa_set_max_gain(uint8_t arg) {
	char buf;
	tpa2016d2_read_register(AGC_REG2, &buf);
	buf&=0x0f;//max gain is the highest 4 bits
	arg&=0xf;
	buf|=arg<<4;
	tpa2016d2_set_register(AGC_REG2, buf);
}

void tpa_set_compression_ratio(uint8_t arg) {
	char buf;
	tpa2016d2_read_register(AGC_REG2, &buf);
	buf&=0xfc;//compression ratio is the lowest 2 bits
	arg&=3;
	buf|=arg;
	tpa2016d2_set_register(AGC_REG2, buf);
}

void enable_speaker_rhod(void) {
	if(!machine_is_htcrhodium())
		return;
	tpa2016d2_set_power(true);
	//Default is use wince's setting
	tpa_set_power(SPK_EN_L|SPK_EN_R);
	tpa_set_attack_time(32/*5.335ms*/);
	tpa_set_release_time(1/*137ms*/);
	tpa_set_hold_time(0/*disabled*/);
	tpa_set_fixed_gain(0x10/*+16dB*/);
	tpa_set_output_limiter(0x19/*6dBV*/);
	tpa_set_max_gain(0xc/*30dB*/);
	tpa_set_compression_ratio(0/*1:1*/);
}

void disable_speaker_rhod(void) {
	if(!machine_is_htcrhodium())
		return;
	tpa2016d2_set_power(false);
}

void speaker_vol_rhod(int arg) {
	//arg ranges from 0 to 5 (0 is supposed to be off I think)
	//Gain ranges from 0 to 30
	int gain=arg*6;
	tpa_set_fixed_gain(gain);
}

//Audience A1010 (sound cancelation, etc)
static int aud_probe(struct i2c_client *client, const struct i2c_device_id *id) {
	//Not much to do here uh ?
	_dat.a1010=client;
	return 0;
}

static const struct i2c_device_id aud_ids[] = {
        { "a1010", 0 },
        { }
};

static struct i2c_driver aud_driver = {
	.driver = {
		.name	= "a1010",
		.owner	= THIS_MODULE,
	},
	.id_table = aud_ids,
	.probe = aud_probe,
};

//ADC3001 ADC + mic bias
static int adc_probe(struct i2c_client *client, const struct i2c_device_id *id) {
	//Not much to do here uh ?
	_dat.adc3001=client;
	return 0;
}

static const struct i2c_device_id adc_ids[] = {
        { "adc3001", 0 },
        { }
};

static struct i2c_driver adc_driver = {
	.driver = {
		.name	= "adc3001",
		.owner	= THIS_MODULE,
	},
	.id_table = adc_ids,
	.probe = adc_probe,
};

static int __init rhod_audio_init(void) {
	int rc;

	if(!machine_is_htcrhodium())
		return 0;

	printk(KERN_INFO "Rhodium audio registering drivers\n");

	rc=i2c_add_driver(&adc_driver);
	if(rc)
		return rc;

	return i2c_add_driver(&aud_driver);
}

module_init(rhod_audio_init);

