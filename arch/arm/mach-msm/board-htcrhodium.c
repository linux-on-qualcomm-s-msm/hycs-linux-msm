/* linux/arch/arm/mach-msm/board-htcrhodium.c
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <linux/android_pmem.h>

#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <linux/i2c.h>
#include <linux/mm.h>

#include <mach/hardware.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/flash.h>
#include <asm/mach/mmc.h>
#include <asm/setup.h>
#include <mach/msm_serial_hs.h>

#include <mach/board.h>
#include <mach/htc_battery.h>
#include <mach/msm_iomap.h>
#include <mach/system.h>
#include <mach/msm_fb.h>
#include <mach/msm_hsusb.h>
#include <mach/vreg.h>

#include <mach/gpio.h>
#include <mach/io.h>
#include <linux/delay.h>
#include <linux/gpio_keys.h>

#ifdef CONFIG_HTC_HEADSET
#include <mach/htc_headset.h>
#endif

#include <linux/microp-keypad.h>
#include <mach/board_htc.h>
#include <mach/tpa2016d2.h>

#include "proc_comm_wince.h"
#include "devices.h"
#include "htc_hw.h"
#include "board-htcrhodium.h"
#include "htc-usb.h"

#if defined(CONFIG_SERIAL_BCM_BT_LPM)
#include <mach/bcm_bt_lpm.h>
#include <linux/serial_core.h>
#endif

static void htcrhodium_device_specific_fixes(void);

extern int init_mmc(void);
static struct resource rhodium_keypad_resources[] = {
	{
		.start = MSM_GPIO_TO_INT(RHODIUM_KPD_IRQ),
		.end = MSM_GPIO_TO_INT(RHODIUM_KPD_IRQ),
		.flags = IORESOURCE_IRQ,
	},
};

static struct microp_keypad_platform_data rhodium_keypad_data = {
	/*
	.clamshell = {
		.gpio = RHODIUM_KB_SLIDER_IRQ,
		.irq = MSM_GPIO_TO_INT(RHODIUM_KB_SLIDER_IRQ),
	},*/
	.backlight_gpio = RHODIUM_BKL_PWR,
};

static struct platform_device rhodium_keypad_device = {
	.name = "microp-keypad",
	.id = 0,
	.num_resources = ARRAY_SIZE(rhodium_keypad_resources),
	.resource = rhodium_keypad_resources,
	.dev = { .platform_data = &rhodium_keypad_data, },
};

#ifdef CONFIG_SERIAL_MSM_HS
static struct msm_serial_hs_platform_data msm_uart_dm2_pdata = {
	.wakeup_irq = MSM_GPIO_TO_INT(21),
	.inject_rx_on_wakeup = 0,
# if defined(CONFIG_SERIAL_BCM_BT_LPM)
	.exit_lpm_cb = bcm_bt_lpm_exit_lpm_locked,
# endif
};

# if defined(CONFIG_SERIAL_BCM_BT_LPM)
static struct bcm_bt_lpm_platform_data bcm_bt_lpm_pdata = {
	.gpio_wake = RHODIUM_BT_WAKE,
	.gpio_host_wake = RHODIUM_BT_HOST_WAKE,
	.request_clock_off_locked = msm_hs_request_clock_off_locked,
	.request_clock_on_locked = msm_hs_request_clock_on_locked,
};

struct platform_device bcm_bt_lpm_device = {
	.name = "bcm_bt_lpm",
	.id = 0,
	.dev = {
		.platform_data = &bcm_bt_lpm_pdata,
	},
};
# endif
#endif

static int usb_phy_init_seq_raph100[] = {
	0x40, 0x31, /* Leave this pair out for USB Host Mode */
	0x1D, 0x0D,
	0x1D, 0x10,
	-1
};

static void usb_phy_shutdown(void)
{
	gpio_set_value(0x64, 1);
	mdelay(3);
	gpio_set_value(0x64, 0);
	mdelay(3);
}

static void usb_phy_reset(void)
{
	usb_phy_shutdown();
	gpio_set_value(0x64, 0);
	mdelay(3);
	gpio_set_value(0x64, 1);
	mdelay(3);
	htc_usb_ulpi_config(1);

}

#if defined(CONFIG_MSM_CAMERA) && defined(CONFIG_MT9T013)

static struct msm_gpio_config camera_off_gpio_table[] = {
    /* CAMERA */
	DEX_GPIO_CFG(0, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT0 */
	DEX_GPIO_CFG(1, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT1 */
	DEX_GPIO_CFG(2, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT2 */
	DEX_GPIO_CFG(3, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT3 */
	DEX_GPIO_CFG(4, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT4 */
	DEX_GPIO_CFG(5, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT5 */
	DEX_GPIO_CFG(6, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT6 */
	DEX_GPIO_CFG(7, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT7 */
	DEX_GPIO_CFG(8, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT8 */
	DEX_GPIO_CFG(9, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT9 */
	DEX_GPIO_CFG(10, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT10 */
	DEX_GPIO_CFG(11, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* DAT11 */
	DEX_GPIO_CFG(12, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* PCLK */
	DEX_GPIO_CFG(13, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* HSYNC_IN */
	DEX_GPIO_CFG(14, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0), /* VSYNC_IN */
	DEX_GPIO_CFG(15, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA, 0), /* MCLK */
};

static struct msm_gpio_config camera_on_gpio_table[] = {
    /* CAMERA */
	DEX_GPIO_CFG(0, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT0 */
	DEX_GPIO_CFG(1, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT1 */
	DEX_GPIO_CFG(2, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT2 */
	DEX_GPIO_CFG(3, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT3 */
	DEX_GPIO_CFG(4, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT4 */
	DEX_GPIO_CFG(5, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT5 */
	DEX_GPIO_CFG(6, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT6 */
	DEX_GPIO_CFG(7, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT7 */
	DEX_GPIO_CFG(8, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT8 */
	DEX_GPIO_CFG(9, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT9 */
	DEX_GPIO_CFG(10, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT10 */
	DEX_GPIO_CFG(11, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT11 */
	DEX_GPIO_CFG(12, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* PCLK */
	DEX_GPIO_CFG(13, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* HSYNC_IN */
	DEX_GPIO_CFG(14, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* VSYNC_IN */
	DEX_GPIO_CFG(15, 1, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA, 0), /* MCLK */
};

static void config_gpio_table(struct msm_gpio_config *table, int len)
{
        int n;
        struct msm_gpio_config id;
        for(n = 0; n < len; n++) {
                id = table[n];
                msm_gpio_set_function( id );
        }
}

static void config_camera_on_gpios(int bFrontCamera)
{
	struct msm_dex_command dex = { .cmd = PCOM_PMIC_REG_ON, .has_data=1 };

    /* VCM VDDD 1.8V */
	dex.data=0x8000;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDDD 1.8V */
	dex.data=0x100;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDDIO 2.65V */
	dex.data=0x400000;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDDA 2.85V */
	dex.data=0x20;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);

    config_gpio_table(camera_on_gpio_table,
            ARRAY_SIZE(camera_on_gpio_table));

    if ( (get_machine_variant_type() == MACHINE_VARIANT_RHOD_4XX) ||
         (get_machine_variant_type() == MACHINE_VARIANT_RHOD_5XX) ) {
        /* GPIO 26 is used to power up sensor */
        printk("%s CDMA\n", __func__);
        msm_gpio_set_function( DEX_GPIO_CFG(RHODIUM_CAM_PWR1_CDMA, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );
    } else {
        /* GPIO 30 is used to select sensor (1 == Front sensor, 0 = Back sensor) */
        printk("%s NON-CDMA\n", __func__);
        msm_gpio_set_function( DEX_GPIO_CFG(RHODIUM_CAM_PWR1     , 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, bFrontCamera) );
    }
    msm_gpio_set_function( DEX_GPIO_CFG(107                 , 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );  // Unknown function
    msm_gpio_set_function( DEX_GPIO_CFG(RHODIUM_MT9T013_RST , 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );  // Used in DevPowerUpMainSensor (reset pin)
    msm_gpio_set_function( DEX_GPIO_CFG(RHODIUM_VGACAM_RST  , 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );  // Used in DevPowerUpVideoSensor (reset pin)
    msm_gpio_set_function( DEX_GPIO_CFG(RHODIUM_VCM_PDP     , 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_2MA, 0) );  // Used in DevPullVCMPDPin
}

static void config_camera_off_gpios(void)
{
    struct msm_dex_command dex = { .cmd = PCOM_PMIC_REG_OFF, .has_data=1 };

    /* VCM VDDD 1.8V */
	dex.data=0x8000;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDDD 1.8V */
	dex.data=0x100;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDDIO 2.65V */
	dex.data=0x400000;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDDA 2.85V */
	dex.data=0x20;
	msm_proc_comm_wince(&dex,0);
    mdelay(20);

    config_gpio_table(camera_off_gpio_table,
            ARRAY_SIZE(camera_off_gpio_table));

    if ( (get_machine_variant_type() == MACHINE_VARIANT_RHOD_4XX) ||
         (get_machine_variant_type() == MACHINE_VARIANT_RHOD_5XX) ) {
        /* Power off camera sensor */
        gpio_direction_output(RHODIUM_CAM_PWR1_CDMA, 1);
    } else {
        /* Reset camera select gpio to main (back) camera */
        gpio_direction_output(RHODIUM_CAM_PWR1, 1);
    }
}

#ifdef CONFIG_MT9T013
static void config_camera_on_gpios_mt9t013(void)
{
    config_camera_on_gpios(0);
}
#endif

// Will be used by front sensor
#if 0
static void config_camera_on_gpios_mt9v113(void)
{
    config_camera_on_gpios(1);
}
#endif

static struct msm_camera_device_platform_data msm_camera_mt9t013_device_data = {
        .camera_gpio_on  = config_camera_on_gpios_mt9t013,
        .camera_gpio_off = config_camera_off_gpios,
        .ioext.mdcphy = MSM_MDC_PHYS,
        .ioext.mdcsz  = MSM_MDC_SIZE,
        .ioext.appphy = MSM_CLK_CTL_PHYS,
        .ioext.appsz  = MSM_CLK_CTL_SIZE,
};

#ifdef CONFIG_MT9T013
static struct msm_camera_sensor_info msm_camera_sensor_mt9t013_data = {
        .sensor_name    = "mt9t013",
        .sensor_reset   = RHODIUM_MT9T013_RST,
//        .sensor_pwd     = RHODIUM_CAM_PWR1, // Only used for ov8810 & ov9665 sensors
        .vcm_pwd        = RHODIUM_VCM_PDP,
        .vcm_reversed_polarity = 1,
        .pdata          = &msm_camera_mt9t013_device_data,
};

static struct platform_device msm_camera_sensor_mt9t013 = {
        .name           = "msm_camera_mt9t013",
        .dev            = {
                .platform_data = &msm_camera_sensor_mt9t013_data,
        },
};

#endif
#endif


static struct platform_device raphael_rfkill = {
	.name = "htcraphael_rfkill",
	.id = -1,
};

static struct tpa2016d2_platform_data tpa2016d2_data = {
    .gpio_tpa2016_spk_en = RHODIUM_SPKR_PWR,
};

static struct i2c_board_info i2c_devices[] = {
	{
		//gsensor 0x38
		I2C_BOARD_INFO("bma150", 0x70>>1),
	},
	{
		// LED & Backlight controller
		I2C_BOARD_INFO("microp-klt", 0x66),
	},
	{
		// Keyboard controller for RHOD
		I2C_BOARD_INFO("microp-ksc", 0x67),
	},
#if defined(CONFIG_MSM_CAMERA) && defined(CONFIG_MT9T013)
	{
		I2C_BOARD_INFO("mt9t013", 0x6C),
	},
#endif
	{		
		I2C_BOARD_INFO("tpa2016d2", 0xb0>>1),
		.platform_data = &tpa2016d2_data,
	},
	{		
		I2C_BOARD_INFO("a1010", 0xf4>>1),
	},
	{		
		I2C_BOARD_INFO("adc3001", 0x30>>1),
	},
};

#define SND(num, desc) { .name = desc, .id = num }
static struct snd_endpoint snd_endpoints_list[] = {
	SND(0, "HANDSET"),
	SND(1, "SPEAKER"),
	SND(2, "HEADSET"),
	SND(3, "BT"),
	SND(44, "BT_EC_OFF"),
	SND(10, "HEADSET_AND_SPEAKER"),
	SND(256, "CURRENT"),

};
#undef SND

static struct msm_snd_endpoints blac_snd_endpoints = {
        .endpoints = snd_endpoints_list,
        .num = ARRAY_SIZE(snd_endpoints_list),
};

static struct platform_device blac_snd = {
	.name = "msm_snd",
	.id = -1,
	.dev	= {
		.platform_data = &blac_snd_endpoints,
	},
};

static struct platform_device rhod_prox = {
    .name       = "rhodium_proximity",
};

static struct platform_device touchscreen = {
	.name		= "tssc-manager",
	.id		= -1,
};

#ifdef CONFIG_HTC_HEADSET

static void h2w_config_cpld(int route);
static void h2w_init_cpld(void);
static struct h2w_platform_data rhodium_h2w_data = {
	.cable_in1		= RHODIUM_CABLE_IN1,
	.cable_in2		= RHODIUM_CABLE_IN2,
	.h2w_clk		= RHODIUM_H2W_CLK,
	.h2w_data		= RHODIUM_H2W_DATA,
	.debug_uart 		= H2W_UART3,
	.config_cpld 		= h2w_config_cpld,
	.init_cpld 		= h2w_init_cpld,
	.headset_mic_35mm	= 17,
};

static void h2w_config_cpld(int route)
{
	switch (route) {
	case H2W_UART3:
		gpio_set_value(RHODIUM_H2W_UART_MUX, 1);
		break;
	case H2W_GPIO:
		gpio_set_value(RHODIUM_H2W_UART_MUX, 0);
		break;
	}
}

static void h2w_init_cpld(void)
{
	h2w_config_cpld(H2W_UART3);
	gpio_set_value(rhodium_h2w_data.h2w_clk, 0);
	gpio_set_value(rhodium_h2w_data.h2w_data, 0);
}

static struct platform_device rhodium_h2w = {
	.name		= "h2w",
	.id		= -1,
	.dev		= {
		.platform_data	= &rhodium_h2w_data,
	},
};
#endif

static struct platform_device *devices[] __initdata = {
	&msm_device_smd,
	&msm_device_nand,
	&msm_device_i2c,
	&msm_device_rtc,
	&msm_device_htc_hw,
	&msm_device_htc_battery,
#ifdef CONFIG_MT9T013
	&msm_camera_sensor_mt9t013,
#endif
	&blac_snd,
	&rhodium_keypad_device,
	&touchscreen,
	&raphael_rfkill,
#ifdef CONFIG_SERIAL_MSM_HS
	&msm_device_uart_dm2,
#endif
	&rhod_prox,
#ifdef CONFIG_HTC_HEADSET
	&rhodium_h2w,
#endif
#ifdef CONFIG_SERIAL_BCM_BT_LPM
	&bcm_bt_lpm_device,
#endif
};

extern struct sys_timer msm_timer;

static void __init halibut_init_irq(void)
{
	msm_init_irq();
}

static struct msm_acpu_clock_platform_data halibut_clock_data = {
	.acpu_switch_time_us = 50,
	.max_speed_delta_khz = 256000,
	.vdd_switch_time_us = 62,
	.power_collapse_khz = 19200,
	.wait_for_irq_khz = 128000,
	.max_axi_khz = 160000,
};

void msm_serial_debug_init(unsigned int base, int irq, 
			   const char *clkname, int signal_irq);

//TODO: use platform data
int wifi_set_power(int on, unsigned long msec) {
	if(machine_is_htcrhodium()) {
		gpio_configure(25, GPIOF_OWNER_ARM11);
		gpio_direction_output(25, 0);
		msleep(0x64);
		gpio_direction_output(25, 1);
	}
	return 0;
}

int wifi_get_irq_number(int on, unsigned long msec) {
	return gpio_to_irq(29);
}

static htc_hw_pdata_t msm_htc_hw_pdata = {
	.set_vibrate = msm_proc_comm_wince_vibrate,
	.battery_smem_offset = 0xfc140,
	.battery_smem_field_size = 4,
};

static smem_batt_t msm_battery_pdata = {
	.gpio_battery_detect = RHODIUM_BAT_IRQ,
	.gpio_charger_enable = RHODIUM_CHARGE_EN_N,
	.gpio_charger_current_select = RHODIUM_USB_AC_PWR,
	.smem_offset = 0xfc140,
	.smem_field_size = 4,
};

static void __init halibut_init(void)
{
	// Fix data in arrays depending on GSM/CDMA version
	htcrhodium_device_specific_fixes();

	msm_acpu_clock_init(&halibut_clock_data);
	msm_proc_comm_wince_init();

	msm_device_htc_hw.dev.platform_data = &msm_htc_hw_pdata;
	msm_device_htc_battery.dev.platform_data = &msm_battery_pdata;

#ifdef CONFIG_SERIAL_MSM_HS
	msm_device_uart_dm2.dev.platform_data = &msm_uart_dm2_pdata;
#endif
	
	platform_add_devices(devices, ARRAY_SIZE(devices));
	i2c_register_board_info(0, i2c_devices, ARRAY_SIZE(i2c_devices));
	msm_add_usb_devices(usb_phy_reset, NULL, usb_phy_init_seq_raph100);
	
	init_mmc();

	msm_init_pmic_vibrator();

	/* TODO: detect vbus and correctly notify USB about its presence 
	 * For now we just declare that VBUS is present at boot and USB
	 * copes, but this is not ideal.
	 */
	msm_hsusb_set_vbus_state(1);

	msm_proc_comm_wince_vibrate_welcome();
}

static void __init halibut_map_io(void)
{
	msm_map_common_io();
	msm_clock_init();
}

//#define RHODIUM_USE_SMI2  /* CAUTION : if enabled here, must also be enabled in pmem.c */

static void __init htcrhodium_fixup(struct machine_desc *desc, struct tag *tags,
                                    char **cmdline, struct meminfo *mi)
{
	mi->nr_banks = 1;
	mi->bank[0].start = PAGE_ALIGN(PHYS_OFFSET);
	mi->bank[0].node = PHYS_TO_NID(mi->bank[0].start);
	mi->bank[0].size = (99 * 1024 * 1024);

	mi->nr_banks++;
	mi->bank[1].start = PAGE_ALIGN(PHYS_OFFSET + 0x10000000);
	mi->bank[1].node = PHYS_TO_NID(mi->bank[1].start);
#ifndef RHODIUM_USE_SMI2
	mi->bank[1].size = (128 * 1024 * 1024)-50*1024*1024; // See pmem.c
#else
	mi->bank[1].size = (128 * 1024 * 1024)-42*1024*1024; // See pmem.c

	//mi->nr_banks++;   // Don't make it available for allocation
	mi->bank[2].start = PAGE_ALIGN(0x02000000+(8)*1024*1024);
	mi->bank[2].node = PHYS_TO_NID(mi->bank[2].start);
	mi->bank[2].size = (32-8/*camera*/)*1024*1024;
#endif

	printk(KERN_INFO "fixup: nr_banks = %d\n", mi->nr_banks);
	printk(KERN_INFO "fixup: bank0 start=%08lx, node=%08x, size=%08lx\n", mi->bank[0].start, mi->bank[0].node, mi->bank[0].size);
	printk(KERN_INFO "fixup: bank1 start=%08lx, node=%08x, size=%08lx\n", mi->bank[1].start, mi->bank[1].node, mi->bank[1].size);
#ifdef RHODIUM_USE_SMI2
	printk(KERN_INFO "fixup: bank2 start=%08lx, node=%08x, size=%08lx\n", mi->bank[2].start, mi->bank[2].node, mi->bank[2].size);
#endif
}

static void htcrhodium_device_specific_fixes(void)
{
	msm_htc_hw_pdata.battery_smem_offset = 0xfc110;
	msm_htc_hw_pdata.battery_smem_field_size = 2;
	msm_battery_pdata.smem_offset = 0xfc110;
	msm_battery_pdata.smem_field_size = 2;
}

MACHINE_START(HTCRHODIUM, "HTC Rhodium cellphone")
	.fixup 		= htcrhodium_fixup,
	.boot_params	= 0x10000100,
	.map_io		= halibut_map_io,
	.init_irq	= halibut_init_irq,
	.init_machine	= halibut_init,
	.timer		= &msm_timer,
MACHINE_END
