/* linux/arch/arm/mach-msm/board-htctopaz.h
 */
#ifndef __ARCH_ARM_MACH_MSM_BOARD_HTCRHODIUM_H
#define __ARCH_ARM_MACH_MSM_BOARD_HTCRHODIUM_H

#include <mach/board.h>


#define DECLARE_MSM_IOMAP
#include <mach/msm_iomap.h>

#define RHODIUM_CAM_DAT0        0
#define RHODIUM_CAM_DAT1        1
#define RHODIUM_CAM_DAT2        2
#define RHODIUM_CAM_DAT3        3
#define RHODIUM_CAM_DAT4        4
#define RHODIUM_CAM_DAT5        5
#define RHODIUM_CAM_DAT6        6
#define RHODIUM_CAM_DAT7        7
#define RHODIUM_CAM_DAT8        8
#define RHODIUM_CAM_DAT9        9
#define RHODIUM_CAM_DAT10       10
#define RHODIUM_CAM_DAT11       11
#define RHODIUM_CAM_PCLK        12
#define RHODIUM_CAM_HSYNC_IN    13
#define RHODIUM_CAM_VSYNC_IN    14
#define RHODIUM_CAM_MCLK        15

#define RHODIUM_CAM_PWR1_CDMA   26
#define RHODIUM_CAM_PWR1        30
#define RHODIUM_VCM_PDP         33
#define RHODIUM_MT9T013_RST     34
#define RHODIUM_VGACAM_RST      109

#define RHODIUM_BT_SHUTDOWN_N   31
#define RHODIUM_BT_nRST         91
#define RHODIUM_BT_HOST_WAKE    94
#define RHODIUM_BT_WAKE         35

#define RHODIUM_PCM_DOUT        68
#define RHODIUM_PCM_DIN         69
#define RHODIUM_PCM_SYNC        70
#define RHODIUM_PCM_CLK         71

#define RHODIUM_CABLE_IN1		42
#define RHODIUM_CABLE_IN2		45
#define RHODIUM_H2W_CLK			46
#define RHODIUM_H2W_DATA		92
#define RHODIUM_H2W_UART_MUX		103	/* pretty sure this is right */

#define RHODIUM_BAT_IRQ			28  // GPIO IRQ
#define RHODIUM_USB_AC_PWR		32
#define RHODIUM_CHARGE_EN_N		44
#define RHODIUM_KPD_IRQ			27  //Keyboard IRQ
#define RHODIUM_KB_SLIDER_IRQ		37  //Keyboard Slider IRQ //Currently Unknown, using stylus detect GPIO right now (37).
#define RHODIUM_BKL_PWR			86  //Keyboard blacklight  //Currently Unknown if this is right

#define RHODIUM_END_KEY			18
#define RHODIUM_VOLUMEUP_KEY		39
#define RHODIUM_VOLUMEDOWN_KEY		40
#define RHODIUM_POWER_KEY		83  //Power key
#define RHODIUM_SPKR_PWR        84
#define RHODIUM_HS_AMP_PWR      85

#define RHOD_LCD_PWR			0x63
#define RHOD_LCD_VSYNC			97
#define RHODIUM_USBPHY_RST		100



#endif 
