/* linux/arch/arm/mach-msm/board-htctopaz.c
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <linux/android_pmem.h>

#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <linux/i2c.h>
#include <linux/mm.h>

#include <mach/hardware.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/flash.h>
#include <asm/mach/mmc.h>
#include <asm/setup.h>
#include <mach/msm_serial_hs.h>

#include <mach/board.h>
#include <mach/htc_battery.h>
#include <mach/msm_iomap.h>
#include <mach/system.h>
#include <mach/msm_fb.h>
#include <mach/msm_hsusb.h>
#include <mach/vreg.h>

#include <mach/gpio.h>
#include <mach/io.h>
#include <linux/delay.h>
#include <linux/gpio_keys.h>

#include <linux/microp-keypad.h>
#include <mach/board_htc.h>
#include <mach/htc_headset.h>

#include "proc_comm_wince.h"
#include "devices.h"
#include "htc_hw.h"
#include "board-htctopaz.h"
#include "htc-usb.h"

static void htctopaz_device_specific_fixes(void);

extern int init_mmc(void);

#ifdef CONFIG_SERIAL_MSM_HS
static struct msm_serial_hs_platform_data msm_uart_dm2_pdata = {
	.wakeup_irq = MSM_GPIO_TO_INT(21),
	.inject_rx_on_wakeup = 1,
	.rx_to_inject = 0x32,
};
#endif

static int usb_phy_init_seq_raph100[] = {
	0x40, 0x31, /* Leave this pair out for USB Host Mode */
	0x1D, 0x0D,
	0x1D, 0x10,
	-1
};

static void usb_phy_shutdown(void)
{
	/* is mdelay needed ? */
        gpio_set_value(TOPA100_USBPHY_RST, 1);
        mdelay(1);
        gpio_set_value(TOPA100_USBPHY_RST, 0);
        mdelay(1);
}
static void usb_phy_reset(void)
{
	usb_phy_shutdown();
	gpio_set_value(TOPA100_USBPHY_RST, 0);
	mdelay(1);
	gpio_set_value(TOPA100_USBPHY_RST, 1);
	mdelay(3);
	htc_usb_ulpi_config(1);
}

#if defined(CONFIG_MSM_CAMERA) && defined(CONFIG_MT9P012)

static struct msm_gpio_config camera_off_gpio_table[] = {
    /* CAMERA */
    DEX_GPIO_CFG(0, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT0 */
    DEX_GPIO_CFG(1, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT1 */
    DEX_GPIO_CFG(2, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT2 */
    DEX_GPIO_CFG(3, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT3 */
    DEX_GPIO_CFG(4, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT4 */
    DEX_GPIO_CFG(5, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT5 */
    DEX_GPIO_CFG(6, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT6 */
    DEX_GPIO_CFG(7, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT7 */
    DEX_GPIO_CFG(8, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT8 */
    DEX_GPIO_CFG(9, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT9 */
    //DEX_GPIO_CFG(10, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT10 */
    //DEX_GPIO_CFG(11, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* DAT11 */
    DEX_GPIO_CFG(12, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* PCLK */
    DEX_GPIO_CFG(13, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* HSYNC_IN */
    DEX_GPIO_CFG(14, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* VSYNC_IN */
    DEX_GPIO_CFG(15, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_4MA, 0), /* MCLK */
};

static struct msm_gpio_config camera_on_gpio_table[] = {
    /* CAMERA */
    DEX_GPIO_CFG(0, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT0 */
    DEX_GPIO_CFG(1, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT1 */
    DEX_GPIO_CFG(2, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT2 */
    DEX_GPIO_CFG(3, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT3 */
    DEX_GPIO_CFG(4, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT4 */
    DEX_GPIO_CFG(5, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT5 */
    DEX_GPIO_CFG(6, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT6 */
    DEX_GPIO_CFG(7, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT7 */
    DEX_GPIO_CFG(8, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT8 */
    DEX_GPIO_CFG(9, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT9 */
    //DEX_GPIO_CFG(10, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT10 */
    //DEX_GPIO_CFG(11, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* DAT11 */
    DEX_GPIO_CFG(12, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* PCLK */
    DEX_GPIO_CFG(13, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* HSYNC_IN */
    DEX_GPIO_CFG(14, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA, 0), /* VSYNC_IN */
    DEX_GPIO_CFG(15, 1, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA, 0), /* MCLK */
};

static void config_gpio_table(struct msm_gpio_config *table, int len)
{
    int n;
    struct msm_gpio_config id;
    for(n = 0; n < len; n++) {
        id = table[n];
        msm_gpio_set_function( id );
    }
}

static void config_camera_on_gpios(int Gpio30Value)
{
    struct msm_dex_command dex = { .cmd = PCOM_PMIC_REG_ON, .has_data=1 };

    /* VDDD 1.8V */
    dex.data=0x100;
    msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDDIO 2.65V */
    dex.data=0x400000;
    msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDDA 2.85V */
    dex.data=0x20;
    msm_proc_comm_wince(&dex,0);
    mdelay(20);

    /* Select camera (front or back) */
    gpio_direction_output(TOPA100_CAM_PWR1, Gpio30Value);

    config_gpio_table(camera_on_gpio_table,
        ARRAY_SIZE(camera_on_gpio_table));
}

static void config_camera_off_gpios(void)
{
    struct msm_dex_command dex = { .cmd = PCOM_PMIC_REG_OFF, .has_data=1 };

    /* VDDD 1.8V */
    dex.data=0x100;
    msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDDIO 2.65V */
    dex.data=0x400000;
    msm_proc_comm_wince(&dex,0);
    mdelay(20);
    /* VDDA 2.85V */
    dex.data=0x20;
    msm_proc_comm_wince(&dex,0);
    mdelay(20);

    /* Reset camera select gpio to main (back) camera */
    gpio_direction_output(TOPA100_CAM_PWR1, 1);

    config_gpio_table(camera_off_gpio_table,
        ARRAY_SIZE(camera_off_gpio_table));
}

#ifdef CONFIG_MT9P012
static void config_camera_on_gpios_mt9p012(void)
{
    config_camera_on_gpios(0);
}
#endif

#endif

static struct i2c_board_info i2c_devices[] = {
	{
		// LED & Backlight controller
		I2C_BOARD_INFO("microp-klt", 0x66),
	},
#if defined(CONFIG_MSM_CAMERA) && defined(CONFIG_MT9P012)
	{		
		I2C_BOARD_INFO("mt9p012", 0x6c >> 1),
	},
#endif
};

#define SND(num, desc) { .name = desc, .id = num }
static struct snd_endpoint snd_endpoints_list[] = {
	SND(0, "HANDSET"),
	SND(1, "SPEAKER"),
	SND(2, "HEADSET"),
	SND(3, "BT"),
	SND(44, "BT_EC_OFF"),
	SND(10, "HEADSET_AND_SPEAKER"),
	SND(256, "CURRENT"),

};
#undef SND

static struct msm_snd_endpoints topaz_snd_endpoints = {
        .endpoints = snd_endpoints_list,
        .num = ARRAY_SIZE(snd_endpoints_list),
};

static struct platform_device topaz_snd = {
	.name = "msm_snd",
	.id = -1,
	.dev	= {
		.platform_data = &topaz_snd_endpoints,
	},
};

#ifdef CONFIG_HTC_HEADSET

static void h2w_config_cpld(int route);
static void h2w_init_cpld(void);
static struct h2w_platform_data topaz_h2w_data = {
	.cable_in1              = TOPA100_CABLE_IN1,
	.cable_in2              = TOPA100_CABLE_IN2,
	.h2w_clk                = TOPA100_H2W_CLK,
	.h2w_data               = TOPA100_H2W_DATA,
	.debug_uart             = H2W_UART3,
	.config_cpld            = h2w_config_cpld,
	.init_cpld              = h2w_init_cpld,
	.headset_mic_35mm       = TOPA100_AUD_HSMIC_DET_N,
};

static void h2w_config_cpld(int route)
{
	printk(KERN_INFO "htctopaz %s: route=%d TODO\n",
		__func__, route);
	switch (route) {
		case H2W_UART3:
			//gpio_set_value(0, 1); 	/*TODO wrong GPIO*/
			break;
		case H2W_GPIO:
			//gpio_set_value(0, 0); 	/*TODO wrong GPIO*/
			break;
	}
}


static void h2w_init_cpld(void)
{
	h2w_config_cpld(H2W_UART3);
	gpio_set_value(topaz_h2w_data.h2w_clk, 0);
	gpio_set_value(topaz_h2w_data.h2w_data, 0);
}

static struct platform_device topaz_h2w = {
	.name           = "h2w",
	.id             = -1,
	.dev            = {
		.platform_data  = &topaz_h2w_data,
	},
};
#endif

static struct platform_device topaz_bt_rfkill = {
	.name = "htcraphael_rfkill",
	.id = -1,
};

static struct platform_device touchscreen = {
	.name		= "tssc-manager",
	.id		= -1,
};

#ifdef CONFIG_MSM_CAMERA

#ifdef CONFIG_MT9P012
static struct msm_camera_device_platform_data msm_camera_device_data_mt9p012 = {
    .camera_gpio_on  = config_camera_on_gpios_mt9p012,
    .camera_gpio_off = config_camera_off_gpios,
    .ioext.mdcphy = MSM_MDC_PHYS,
    .ioext.mdcsz  = MSM_MDC_SIZE,
    .ioext.appphy = MSM_CLK_CTL_PHYS,
    .ioext.appsz  = MSM_CLK_CTL_SIZE,
};

static struct msm_camera_sensor_info msm_camera_sensor_mt9p012_data = {
    .sensor_name    = "mt9p012",
    .sensor_reset   = TOPA100_CAM1_RST,
    .sensor_pwd     = TOPA100_CAM_PWR1,
    .vcm_pwd        = TOPA100_CAM_VCMPDP,
    .pdata          = &msm_camera_device_data_mt9p012,
};

static struct platform_device msm_camera_sensor_mt9p012 = {
    .name           = "msm_camera_mt9p012",
    .dev            = {
	    .platform_data = &msm_camera_sensor_mt9p012_data,
    },
};
#endif

#endif

static struct platform_device *devices[] __initdata = {
	&msm_device_smd,
	&msm_device_nand,
	&msm_device_i2c,
	&msm_device_rtc,
	&msm_device_htc_hw,
	&msm_device_htc_battery,
	&topaz_snd,		
#ifdef CONFIG_HTC_HEADSET
	&topaz_h2w,
#endif
	&topaz_bt_rfkill,
	&touchscreen,
#ifdef CONFIG_SERIAL_MSM_HS
	&msm_device_uart_dm2,
#endif
#ifdef CONFIG_MT9P012
	&msm_camera_sensor_mt9p012,
#endif
};

extern struct sys_timer msm_timer;

static void __init halibut_init_irq(void)
{
	msm_init_irq();
}

static struct msm_acpu_clock_platform_data halibut_clock_data = {
	.acpu_switch_time_us = 50,
	.max_speed_delta_khz = 256000,
	.vdd_switch_time_us = 62,
	.power_collapse_khz = 19200,
	.wait_for_irq_khz = 128000,
	.max_axi_khz = 160000,
};

void msm_serial_debug_init(unsigned int base, int irq, 
			   const char *clkname, int signal_irq);

static htc_hw_pdata_t msm_htc_hw_pdata = {
	.set_vibrate = msm_proc_comm_wince_vibrate,
	.battery_smem_offset = 0xfc140,
	.battery_smem_field_size = 4,
};

static smem_batt_t msm_battery_pdata = {
	.gpio_battery_detect = TOPA100_BAT_IN,
	.gpio_charger_enable = TOPA100_CHARGE_EN_N,
	.gpio_charger_current_select = TOPA100_USB_AC_PWR,
	.smem_offset = 0xfc140,
	.smem_field_size = 4,
};

static void __init halibut_init(void)
{
	// Fix data in arrays depending on GSM/CDMA version
	htctopaz_device_specific_fixes();

	msm_acpu_clock_init(&halibut_clock_data);
	msm_proc_comm_wince_init();

	msm_device_htc_hw.dev.platform_data = &msm_htc_hw_pdata;
	msm_device_htc_battery.dev.platform_data = &msm_battery_pdata;
	
	platform_add_devices(devices, ARRAY_SIZE(devices));
	i2c_register_board_info(0, i2c_devices, ARRAY_SIZE(i2c_devices));
	msm_add_usb_devices(usb_phy_reset, NULL, usb_phy_init_seq_raph100);
	init_mmc();
#ifdef CONFIG_SERIAL_MSM_HS
	msm_device_uart_dm2.dev.platform_data = &msm_uart_dm2_pdata;
#endif
	msm_init_pmic_vibrator();

	/* TODO: detect vbus and correctly notify USB about its presence 
	 * For now we just declare that VBUS is present at boot and USB
	 * copes, but this is not ideal.
	 */
	msm_hsusb_set_vbus_state(1);

	msm_proc_comm_wince_vibrate_welcome();
}

static void __init halibut_map_io(void)
{
	msm_map_common_io();
	msm_clock_init();
}

static void __init htctopaz_fixup(struct machine_desc *desc, struct tag *tags,
                                    char **cmdline, struct meminfo *mi)
{
	parse_tag_monodie((const struct tag *)tags);

	mi->nr_banks = 2;
	mi->bank[0].start = PAGE_ALIGN(PHYS_OFFSET);
	mi->bank[0].node = PHYS_TO_NID(mi->bank[0].start);
	mi->bank[0].size = (104 * 1024 * 1024);

	if (board_mcp_monodie()) {
		mi->bank[1].start = PAGE_ALIGN(PHYS_OFFSET + 0x08000000);
	} else {
		mi->bank[1].start = PAGE_ALIGN(PHYS_OFFSET + 0x10000000);
	}
	mi->bank[1].node = PHYS_TO_NID(mi->bank[1].start);
	mi->bank[1].size = (128 * 1024 * 1024)-50*1024*1024; // See pmem.c

	printk(KERN_INFO "fixup: nr_banks = %d\n", mi->nr_banks);
	printk(KERN_INFO "fixup: bank0 start=%08lx, node=%08x, size=%08lx\n", mi->bank[0].start, mi->bank[0].node, mi->bank[0].size);
	printk(KERN_INFO "fixup: bank1 start=%08lx, node=%08x, size=%08lx\n", mi->bank[1].start, mi->bank[1].node, mi->bank[1].size);
}

static void htctopaz_device_specific_fixes(void)
{
	msm_htc_hw_pdata.battery_smem_offset = 0xfc110;
	msm_htc_hw_pdata.battery_smem_field_size = 2;
	msm_battery_pdata.smem_offset = 0xfc110;
	msm_battery_pdata.smem_field_size = 2;
}

MACHINE_START(HTCTOPAZ, "HTC Topaz cellphone (Topaz is a silicate mineral of aluminium and fluorine)")
	.fixup 		= htctopaz_fixup,
	.boot_params	= 0x10000100,
	.map_io		= halibut_map_io,
	.init_irq	= halibut_init_irq,
	.init_machine	= halibut_init,
	.timer		= &msm_timer,
MACHINE_END
