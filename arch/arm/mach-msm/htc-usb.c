/* linux/arch/arm/mach-msm/htc-usb.c
 *
 * Copyright (C) 2010 htc-linux.org
 * Author(s): Alexander Tarasikov <alexander.tarasikov@gmail.com>, Abel Laura <abel.laura@gmail.com>
 *
 * based on devices.c by HTC Corporation
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include "htc-usb.h"

//#ifdef CONFIG_DEBUG_INFO
#define _D printk
//#else
//#define _D while(0) printk
//#endif

static struct work_struct htc_cable_notify_work;
static int usb_connected_stat = 0;

void htc_bat_notify_usb_connected(int online);

void htc_usb_update_vbus(void) {
	msm_hsusb_set_vbus_state(!!readl(MSM_SHARED_RAM_BASE+0xfc00c));
}

void htc_usb_ulpi_config(int enable) {
	int n;
	/* Needs modification? this is based on halibut info */
	/* Configure ULPI DATA pins */
	for(n = 0x6f; n <= 0x76; n++) {
		msm_gpio_set_function(DEX_GPIO_CFG(n, 1,
		enable ? GPIO_INPUT : GPIO_OUTPUT, 
		enable ? GPIO_NO_PULL : GPIO_PULL_DOWN,
		GPIO_2MA, 0));
 	}
	msm_gpio_set_function(DEX_GPIO_CFG(0x77, 1, GPIO_INPUT,
		enable ? GPIO_NO_PULL : GPIO_PULL_DOWN, GPIO_2MA, 0));
	msm_gpio_set_function(DEX_GPIO_CFG(0x78, 1, GPIO_INPUT,
		enable ? GPIO_NO_PULL : GPIO_PULL_DOWN, GPIO_2MA, 0));
	msm_gpio_set_function(DEX_GPIO_CFG(0x79, 1, GPIO_OUTPUT,
		enable ? GPIO_NO_PULL : GPIO_PULL_UP, GPIO_2MA, 0));	
}

/* TODO: determine if its best to leave this info here
 * or the board files. Currently init seq is in each 
 * boardfile */
#if 0
static int usb_phy_init_seq_msm72k[] = {
	0x40, 0x31, /* Leave this pair out for USB Host Mode */
	0x1D, 0x0D,
	0x1D, 0x10,
	0x5, 0xA,
	-1
};

static int usb_phy_init_seq_msm72k_cdma[] = {
	0x04, 0x48, /* Host mode is unsure for diam800 */
	0x3A, 0x10,
	0x3B, 0x10,
	-1
};
#endif


/* For what it's worth, the wince driver
 * seems to activate this mode only if it
 * detects some specific AC charger
 * for now, i'll just leave it here
 * maybe some day we will need it
static int usb_phy_init_seq_china_ac[] = {
	0x04, 0x49,
	0x3A, 0x10,
	0x3B, 0x10,
	-1
}; */

static void htc_cable_notify_do_work(struct work_struct *work)
{
    printk("%s: %d\n", __func__, usb_connected_stat);
    htc_bat_notify_usb_connected(usb_connected_stat);
}


static void usb_connected(int on) {
	
	switch (on) {
	case 2: /* ac power? */
		/* TODO : not sure if batt_smem needs to be notified */
	break;
	case 1:	/* usb plugged in ? Nand/haret may have different reactions to this. Needs to be looked into*/
		usb_connected_stat = 1;
		schedule_work(&htc_cable_notify_work);
	break;
	case 0: /* Disconnected ? need to notify */
		usb_connected_stat = 0;
		htc_usb_ulpi_config(0);
		schedule_work(&htc_cable_notify_work);
	break;
	default:
		printk(KERN_WARNING "%s: FIXME! value for ON? %u ?\n", __func__, on);
    	}
	
}

static struct msm_hsusb_platform_data msm_hsusb_pdata;


/***************************************************************
 * Android stuff
 ***************************************************************/
#ifdef CONFIG_USB_ANDROID
static char *usb_functions_ums[] = {
	"usb_mass_storage",
};

static char *usb_functions_ums_adb[] = {
	"usb_mass_storage",
	"adb",
};

static char *usb_functions_rndis[] = {
	"rndis",
};

static char *usb_functions_rndis_adb[] = {
	"rndis",
	"adb",
};

#ifdef CONFIG_USB_ANDROID_DIAG
static char *usb_functions_adb_diag[] = {
	"usb_mass_storage",
	"adb",
	"diag",
};
#endif

static char *usb_functions_all[] = {
#ifdef CONFIG_USB_ANDROID_RNDIS
	"rndis",
#endif
	"usb_mass_storage",
	"adb",
#ifdef CONFIG_USB_ANDROID_ACM
	"acm",
#endif
#ifdef CONFIG_USB_ANDROID_DIAG
	"diag",
#endif
};

static struct android_usb_product usb_products[] = {
	{
		.product_id	= 0x0ffe,
		.num_functions	= ARRAY_SIZE(usb_functions_rndis),
		.functions	= usb_functions_rndis,
	},	
	{
		.product_id	= 0x0c01,
		.num_functions	= ARRAY_SIZE(usb_functions_ums),
		.functions	= usb_functions_ums,
	},
	{
		.product_id	= 0x0c02,
		.num_functions	= ARRAY_SIZE(usb_functions_ums_adb),
		.functions	= usb_functions_ums_adb,
	},
	{
		.product_id	= 0x0ffc,
		.num_functions	= ARRAY_SIZE(usb_functions_rndis_adb),
		.functions	= usb_functions_rndis_adb,
	},
#ifdef CONFIG_USB_ANDROID_DIAG
	{
		.product_id	= 0x0fff,
		.num_functions	= ARRAY_SIZE(usb_functions_adb_diag),
		.functions	= usb_functions_adb_diag,
	},
#endif
};

static struct usb_mass_storage_platform_data mass_storage_pdata = {
	.nluns		= 1,
	.vendor		= "HTC",
	.product	= "XDA",
	.release	= 0x0100,
};

static struct platform_device usb_mass_storage_device = {
	.name	= "usb_mass_storage",
	.id	= -1,
	.dev	= {
		.platform_data = &mass_storage_pdata,
	},
};

#ifdef CONFIG_USB_ANDROID_RNDIS
static struct usb_ether_platform_data rndis_pdata = {
	/* ethaddr is filled by board_serialno_setup */
	.vendorID	= 0x18d1,
	.vendorDescr	= "HTC",
};

static struct platform_device rndis_device = {
	.name	= "rndis",
	.id	= -1,
	.dev	= {
		.platform_data = &rndis_pdata,
	},
};
#endif


static struct android_usb_platform_data android_usb_pdata = {
	.vendor_id	= 0x0bb4,
	.product_id	= 0x0c01,
	.version	= 0x0100,
	.serial_number		= "000000000000",
	.product_name		= "XDA",
	.manufacturer_name	= "HTC",
	.num_products = ARRAY_SIZE(usb_products),
	.products = usb_products,
	.num_functions = ARRAY_SIZE(usb_functions_all),
	.functions = usb_functions_all,
};

static struct platform_device android_usb_device = {
	.name	= "android_usb",
	.id		= -1,
	.dev		= {
		.platform_data = &android_usb_pdata,
	},
};
#endif

/***************************************************************
 * End of android stuff
 ***************************************************************/

int __init msm_add_usb_devices(void (*phy_reset) (void), void (*hw_reset) (void), int *init_seq)
{
	int ret = 0;

	INIT_WORK(&htc_cable_notify_work, htc_cable_notify_do_work);

	/* setup */
	msm_hsusb_pdata.phy_init_seq = init_seq;
	msm_hsusb_pdata.phy_reset = phy_reset;
	/* TODO: hw_reset currently not used but is available.
	 * we will ignore any info passed
	msm_hsusb_pdata.hw_reset = ??; 
	*/
	msm_hsusb_pdata.usb_connected = usb_connected;

	msm_device_hsusb.dev.platform_data = &msm_hsusb_pdata;
	ret = platform_device_register(&msm_device_hsusb);
	if (ret)
		goto exit_or_fail;

	htc_usb_update_vbus();

#ifdef CONFIG_USB_ANDROID
	platform_device_register(&android_usb_device);
#endif
#ifdef CONFIG_USB_ANDROID_RNDIS
	platform_device_register(&rndis_device);
#endif
#ifdef CONFIG_USB_ANDROID_MASS_STORAGE
	platform_device_register(&usb_mass_storage_device);
#endif

exit_or_fail:
	return ret;
}

