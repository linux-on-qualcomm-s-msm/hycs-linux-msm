/* arch/arm/mach-msm/htc-usb.h
 *
 * Copyright (C) 2010 htc-linux.org
 * Author(s): Alexander Tarasikov <alexander.tarasikov@gmail.com>, Abel Laura <abel.laura@gmail.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef __HTC_USB_H__
#define __HTC_USB_H__

#include <linux/platform_device.h>
#include <asm/mach-types.h>
#include <mach/msm_hsusb.h>
#include <mach/msm_iomap.h>
#include <asm/io.h>
#include <mach/board.h>
#include <mach/gpio.h>
#include <linux/delay.h>

#ifdef CONFIG_USB_ANDROID
	#include <linux/usb/android_composite.h>
#endif

#include "proc_comm_wince.h"
#include "devices.h"

void htc_usb_update_vbus(void);
void htc_usb_ulpi_config(int);
int __init msm_add_usb_devices(void (*phy_reset) (void), void (*hw_reset) (void), int *init_seq);
#endif /* __HTC_USB_H__ */
