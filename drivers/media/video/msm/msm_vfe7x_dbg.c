/*
 * Author: Jbruneaux
 *
 * Description : This file prints content of vfe messages
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/msm_adsp.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/android_pmem.h>
#include <mach/msm_adsp.h>
#include <linux/delay.h>
#include <linux/wait.h>
#include "msm_vfe7x.h"

#include <asm/mach-types.h>

#include <mach/qdsp5/qdsp5vfecmdi.h>

char VFE_msg_id_to_name[3][32][32] = {
    {
/* 0 */ "VFE_CMD_RESET",
/* 1 */ "VFE_CMD_START",
/* 2 */ "VFE_CMD_STOP",
/* 3 */ "VFE_CMD_UPDATE",
/* 4 */ "VFE_CMD_CAMIF_CFG",
/* 5 */ "VFE_CMD_ACTIVE_REGION_CFG",
/* 6 */ "VFE_CMD_DEMOSAIC_CFG",
/* 7 */ "VFE_CMD_IP_FORMAT_CFG",
/* 8 */ "VFE_CMD_OP_CLAMP_CFG",
/* 9 */ "VFE_CMD_CHROMA_SUBSAMPLE_CFG",
/* a */ "VFE_CMD_BLACK_LVL_CFG",
/* b */ "VFE_CMD_WHITE_BALANCE_CFG",
/* c */ "VFE_CMD_COLOR_PROCESS_CFG",
/* d */ "VFE_CMD_ADP_FILTER_CFG",
/* e */ "VFE_CMD_FRAME_SKIP_CFG",
/* f */ "VFE_CMD_FOV_CROP",
/* 10*/ "VFE_CMD_STATS_AUTOFOCUS_CFG",
/* 11*/ "VFE_CMD_STATS_WB_EXP_CFG",
/* 12*/ "VFE_CMD_STATS_HG_CFG",
/* 13*/ "VFE_CMD_OP1_ACK",
/* 14*/ "VFE_CMD_OP2_ACK",
/* 15*/ "VFE_CMD_STATS_AF_ACK",
/* 16*/ "VFE_CMD_STATS_WB_EXP_ACK",
/* 17*/ "VFE_CMD_EPOCH1_ACK",
/* 18*/ "VFE_CMD_EPOCH2_ACK",
/* 19*/ "VFE_CMD_SYNC_TIMER1_CFG",
/* 1A*/ "VFE_CMD_SYNC_TIMER2_CFG",
/* 1B*/ "VFE_CMD_ASYNC_TIMER1_START",
/* 1C*/ "VFE_CMD_ASYNC_TIMER2_START",
/* 1D */"VFE_CMD_STATS_AF_UPDATE",
/* 1E */"VFE_CMD_UPDATE_CAMIF_FRAME_CFG",
/* 1F */"VFE_CMD_STATS_WB_EXP_UPDATE",
    },
    {
/* 0 */ "VFE_CMD_SCALE_OP1_CFG",
/* 1 */ "VFE_CMD_SCALE_OP2_CFG",
    },
    {
/* 0 */ "VFE_CMD_AXI_IP_CFG",
/* 1 */ "VFE_CMD_AXI_OP_CFG",
/* 2 */ "VFE_CMD_RGB_GAMMA_CFG",
/* 3 */ "VFE_CMD_Y_GAMMA_CFG",
/* 4 */ "VFE_CMD_ROLLOFF_CFG",
    },
};


/***********************************************************************************
 * Imported from msm_vfe8x
 ***********************************************************************************/


/***********************************************************************************
 * Imported from msm_vfe8x_proc.h
 ***********************************************************************************/
typedef struct {
	/* CAMIF Config */
	uint32_t /* reserved */ : 1;
	uint32_t VSyncEdge:1;
	uint32_t HSyncEdge:1;
	uint32_t syncMode:2;
	uint32_t vfeSubsampleEnable:1;
	 uint32_t /* reserved */ : 1;
	uint32_t busSubsampleEnable:1;
	uint32_t camif2vfeEnable:1;
	 uint32_t /* reserved */ : 1;
	uint32_t camif2busEnable:1;
	uint32_t irqSubsampleEnable:1;
	uint32_t binningEnable:1;
	 uint32_t /* reserved */ : 18;
	uint32_t misrEnable:1;
} __attribute__ ((packed, aligned(4))) VFE_CAMIFConfigType;

typedef struct {
	/* EFS_Config */
	uint32_t efsEndOfLine:8;
	uint32_t efsStartOfLine:8;
	uint32_t efsEndOfFrame:8;
	uint32_t efsStartOfFrame:8;
	/* Frame Config */
	uint32_t frameConfigPixelsPerLine:14;
	 uint32_t /* reserved */ : 2;
	uint32_t frameConfigLinesPerFrame:14;
	 uint32_t /* reserved */ : 2;
	/* Window Width Config */
	uint32_t windowWidthCfgLastPixel:14;
	 uint32_t /* reserved */ : 2;
	uint32_t windowWidthCfgFirstPixel:14;
	 uint32_t /* reserved */ : 2;
	/* Window Height Config */
	uint32_t windowHeightCfglastLine:14;
	 uint32_t /* reserved */ : 2;
	uint32_t windowHeightCfgfirstLine:14;
	 uint32_t /* reserved */ : 2;
	/* Subsample 1 Config */
	uint32_t subsample1CfgPixelSkip:16;
	uint32_t subsample1CfgLineSkip:16;
	/* Subsample 2 Config */
	uint32_t subsample2CfgFrameSkip:4;
	uint32_t subsample2CfgFrameSkipMode:1;
	uint32_t subsample2CfgPixelSkipWrap:1;
	 uint32_t /* reserved */ : 26;
	/* Epoch Interrupt */
	uint32_t epoch1Line:14;
	 uint32_t /* reserved */ : 2;
	uint32_t epoch2Line:14;
	 uint32_t /* reserved */ : 2;
} __attribute__ ((packed, aligned(4))) vfe_camifcfg;

typedef struct {
	/* Demosaic Config */
	uint32_t abfEnable:1;
	uint32_t badPixelCorrEnable:1;
	uint32_t forceAbfOn:1;
	 uint32_t /* reserved */ : 1;
	uint32_t abfShift:4;
	uint32_t fminThreshold:7;
	 uint32_t /* reserved */ : 1;
	uint32_t fmaxThreshold:7;
	 uint32_t /* reserved */ : 5;
	uint32_t slopeShift:3;
	 uint32_t /* reserved */ : 1;
} __attribute__ ((packed, aligned(4))) vfe_demosaic_cfg;

typedef struct {
	/* Demosaic BPC Config 0 */
	uint32_t blueDiffThreshold:12;
	uint32_t redDiffThreshold:12;
	 uint32_t /* reserved */ : 8;
	/* Demosaic BPC Config 1 */
	uint32_t greenDiffThreshold:12;
	 uint32_t /* reserved */ : 20;
} __attribute__ ((packed, aligned(4))) vfe_demosaic_bpc_cfg;

typedef struct {
	/* Demosaic ABF Config 0 */
	uint32_t lpThreshold:10;
	 uint32_t /* reserved */ : 22;
	/* Demosaic ABF Config 1 */
	uint32_t ratio:4;
	uint32_t minValue:10;
	 uint32_t /* reserved */ : 2;
	uint32_t maxValue:10;
	 uint32_t /* reserved */ : 6;
} __attribute__ ((packed, aligned(4))) vfe_demosaic_abf_cfg;

static void vfe_7x_decode_msg_CommandQueue(void *cmd_data, int len)
{
    switch( ((uint32_t*) cmd_data)[0] ) {
        case VFE_CMD_START:
        {
            vfe_cmd_start *cmd = (vfe_cmd_start*)cmd_data;
            printk("startup_params = 0x%08x\nimage_pipeline = 0x%08x\nframe_dimension = 0x%08x\n",
                 cmd->startup_params, cmd->image_pipeline, cmd->frame_dimension);
        }
        break;

        case VFE_CMD_CAMIF_CFG:
        {
            VFE_CAMIFConfigType *camif_conf = (VFE_CAMIFConfigType*) &((uint32_t*)cmd_data)[1];
            vfe_camifcfg        *vfe_cmaif_cfg = (vfe_camifcfg*) &((uint32_t*)cmd_data)[2]; 
            printk("Camif Config : \nVSyncEdge = %d\nHSyncEdge = %d\nsyncMode = %d\nvfeSubsampleEnable = %d\n"
                    "busSubsampleEnable = %d\ncamif2vfeEnable = %d\ncamif2busEnable = %d\nirqSubsampleEnable = %d\n"
                    "binningEnable = %d\nmisrEnable = %d\n", camif_conf->VSyncEdge , camif_conf->HSyncEdge , camif_conf->syncMode ,
                     camif_conf->vfeSubsampleEnable ,camif_conf->busSubsampleEnable , camif_conf->camif2vfeEnable ,
                     camif_conf->camif2busEnable , camif_conf->irqSubsampleEnable , camif_conf->binningEnable , camif_conf->misrEnable);
            printk("VFE Camif conf : \nframeConfigPixelsPerLine = %d\nframeConfigLinesPerFrame = %d\n"
                    "windowWidthCfgLastPixel = %d\nwindowHeightCfgfirstLine = %d\n", vfe_cmaif_cfg->frameConfigPixelsPerLine,
                    vfe_cmaif_cfg->frameConfigLinesPerFrame, vfe_cmaif_cfg->windowWidthCfgLastPixel, vfe_cmaif_cfg->windowHeightCfgfirstLine);
        }
        break;

        case VFE_CMD_DEMOSAIC_CFG:
        {
            vfe_demosaic_cfg *demosaic_cfg = (vfe_demosaic_cfg*) &((uint32_t*)cmd_data)[1];
            vfe_demosaic_abf_cfg *abf_cfg = (vfe_demosaic_abf_cfg*) &((uint32_t*)cmd_data)[2];
            vfe_demosaic_bpc_cfg *bpc_cfg = (vfe_demosaic_bpc_cfg*) &((uint32_t*)cmd_data)[4];
            printk("Demosaic Config : \nabfEnable = %d\nbadPixelCorrEnable = %d\nforceAbfOn = %d\n"
                    "abfShift = %d\nfminThreshold = %d\nfmaxThreshold = %d\nslopeShift = %d\n",
                    demosaic_cfg->abfEnable, demosaic_cfg->badPixelCorrEnable, demosaic_cfg->forceAbfOn,
                    demosaic_cfg->abfShift, demosaic_cfg->fminThreshold, demosaic_cfg->fmaxThreshold, demosaic_cfg->slopeShift);
            printk("adaptive bayer filter config :\nlpThreshold = %d\nratio = %d\nminValue = %d\nmaxValue = %d\n",
                    abf_cfg->lpThreshold, abf_cfg->ratio, abf_cfg->minValue, abf_cfg->maxValue);
            printk("defective pixel correction config:\nblueDiffThreshold = %d\nredDiffThreshold = %d\ngreenDiffThreshold = %d\n",
                    bpc_cfg->blueDiffThreshold, bpc_cfg->redDiffThreshold, bpc_cfg->greenDiffThreshold); 
        }
        break;
    }
}

static void vfe_7x_decode_msg_CommandScaleQueue(void *cmd_data, int len)
{
    switch( ((uint32_t*) cmd_data)[0] ) {

    }
}

static void vfe_7x_decode_msg_CommandTableQueue(void *cmd_data, int len)
{
    switch( ((uint32_t*) cmd_data)[0] ) {

    }
}

void vfe_7x_display_command_id(struct msm_vfe_command_7k *vfecmd, void *cmd_data)
{
	printk("send adsp command = %d, queue = %d            // Send %s\n", *(uint32_t *) cmd_data, vfecmd->queue,
            VFE_msg_id_to_name[vfecmd->queue - 24][*(uint32_t *) cmd_data] );
}

void vfe_7x_decode_msg(struct msm_vfe_command_7k *vfecmd, void *cmd_data, int bDumpRawContent)
{
    int rc;

    if (bDumpRawContent) {
        printk("//////////////////////////////////////////////////////////////////////////////////////////\n");
        for( rc=0; rc<(vfecmd->length / 4); rc++ ) {
            printk("%s : cmd_data[%d] = 0x%08x\n", __func__, rc, ((uint32_t*) cmd_data)[rc]); 
        }
        printk("//////////////////////////////////////////////////////////////////////////////////////////\n");
    }
    
    switch ( vfecmd->queue ) {
        case 24:        /* QDSP_vfeCommandQueue */
            vfe_7x_decode_msg_CommandQueue(cmd_data, vfecmd->length);
        break;

        case 25:        /* QDSP_vfeCommandScaleQueue */
            vfe_7x_decode_msg_CommandScaleQueue(cmd_data, vfecmd->length);
        break;

        case 26:        /* QDSP_vfeCommandTableQueue */
            vfe_7x_decode_msg_CommandTableQueue(cmd_data, vfecmd->length);
        break;  

        default:
            printk("%s: Error : message queue %d is not a valid VFE queue\n",
                __func__, vfecmd->queue);
        break;
    }
}


