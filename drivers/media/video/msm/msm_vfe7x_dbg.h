/*
 * Author: Jbruneaux
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef _MSM_VFE7X_DBG_MSG_
#define _MSM_VFE7X_DBG_MSG_

/* Exported functions */
void vfe_7x_display_command_id(struct msm_vfe_command_7k *vfecmd, void *cmd_data);
void vfe_7x_decode_msg(struct msm_vfe_command_7k *vfecmd, void *cmd_data, int bDumpRawContent);

#endif /* _MSM_VFE7X_DBG_MSG_ */
